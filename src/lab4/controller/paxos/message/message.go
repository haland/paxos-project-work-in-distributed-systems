package message

import "encoding/gob"

var (
	LearnChan    chan Learn
	TrustChan    chan Trust
	AcceptChan   chan Accept
	PrepareChan  chan Prepare
	PromiseChan  chan Promise
	ReceivedChan chan Received
)

func init() {
	gob.Register(Received{})
	gob.Register(Prepare{})
	gob.Register(Promise{})
	gob.Register(Accept{})
	gob.Register(Trust{})
	gob.Register(Learn{})

	LearnChan = make(chan Learn, 5)
	TrustChan = make(chan Trust, 5)
	AcceptChan = make(chan Accept, 5)
	PrepareChan = make(chan Prepare, 5)
	PromiseChan = make(chan Promise, 5)
	ReceivedChan = make(chan Received, 5)
}

type Current struct {
	Round  int
	Value  string
	Leader bool
}

type PackageLoss struct {
	Lost bool
}

type Received struct {
	IP    string
	Value string
}

type Learn struct {
	RoundNumber int
	Value       string
}

type Trust struct {
	IP    string
	Value string
}

type Prepare struct {
	IP          string
	RoundNumber int
}

type Promise struct {
	IP              string
	RoundNumber     int
	LastRoundNumber int
	LastRoundValue  string
}

type Accept struct {
	IP, Value   string
	RoundNumber int
}
