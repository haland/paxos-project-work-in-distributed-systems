package acceptor

import (
	leader "lab4/controller/leader/election"
	paxos "lab4/controller/paxos/message"

	"lab4/model/list"
	"lab4/model/network/tcp"

	"fmt"
)

var (
	_ip          string
	_port        string
	_current     *paxos.Current
	_packageLoss *paxos.PackageLoss

	lastRoundNumber int
	lastRoundValue  string

	_connected = true
	_list      = &list.List
)

func Acceptor(ip, port string, current *paxos.Current, packageLoss *paxos.PackageLoss) {
	_ip = ip
	_port = port
	_current = current

	_packageLoss = packageLoss

	go onAccept()
	go onPrepare()
	go onReceived()
}

func onPrepare() {
	for _connected {
		ack, ok := <-paxos.PrepareChan

		if ok {
			if !_packageLoss.Lost {
				if ack.RoundNumber > _current.Round || (_current.Leader && ack.RoundNumber >= _current.Round) {
					_current.Round = ack.RoundNumber
					fmt.Println("Paxos: Promising slot for value")
					tcp.Send(ack.IP, _port, paxos.Promise{IP: _ip, RoundNumber: _current.Round, LastRoundNumber: lastRoundNumber, LastRoundValue: lastRoundValue})
				}
			}
		} else {
			_connected = false
		}
	}
}

func onAccept() {
	for _connected {
		ack, ok := <-paxos.AcceptChan

		if ok {
			if ack.RoundNumber >= _current.Round && ack.RoundNumber != lastRoundNumber {
				_current.Round = ack.RoundNumber
				lastRoundNumber = ack.RoundNumber
				lastRoundValue = ack.Value

				fmt.Println("Paxos: Accepted the value and sending 'Learn' to nodes.")
				for i := 0; i < len(_list.NodeList); i++ {
					tcp.Send(_list.NodeList[i].IP, _port, paxos.Learn{RoundNumber: ack.RoundNumber, Value: ack.Value})
				}
			}
		} else {
			_connected = false
		}
	}
}

func onReceived() {
	for _connected {
		ack, ok := <-paxos.ReceivedChan

		if ok {
			if leader.LeaderNode.IP != "" {
				fmt.Println("Paxos: Received message from client. Forwarding to Paxos leader.")
				tcp.Send(leader.LeaderNode.IP, _port, paxos.Trust{IP: ack.IP, Value: ack.Value})
			}
		} else {
			_connected = false
		}
	}
}
