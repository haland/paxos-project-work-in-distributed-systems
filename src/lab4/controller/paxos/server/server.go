package server

import (
	failure "lab4/controller/failure/detector"
	leader "lab4/controller/leader/election"

	"lab4/controller/paxos/acceptor"
	"lab4/controller/paxos/learner"
	"lab4/controller/paxos/message"
	"lab4/controller/paxos/proposer"

	"fmt"
)

var (
	current     = message.Current{Round: 0}
	packageLoss = message.PackageLoss{Lost: false}
)

func Server(ip, port string) {
	go leader.Election(ip, port)
	go failure.Detector(ip, port)

	go learner.Learner()
	go acceptor.Acceptor(ip, port, &current, &packageLoss)
	go proposer.Proposer(ip, port, &current)

	for {
		fmt.Println("Paxos: Simulate package loss? (Y/N)")
		var key string
		fmt.Scanf("%s", &key)

		if key == "Y" {
			packageLoss.Lost = true
			fmt.Println("Paxos: Package loss simulation enabled")
		} else if key == "N" {
			packageLoss.Lost = false
			fmt.Println("Paxos: Package loss simulation disabled")
		} else {
			fmt.Println("Paxos: The input was not valid")
		}
	}
}
