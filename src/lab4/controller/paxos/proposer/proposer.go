package proposer

import (
	leader "lab4/controller/leader/message"
	paxos "lab4/controller/paxos/message"

	"lab4/model/list"
	"lab4/model/network/tcp"

	"fmt"
	"strconv"
	"time"
)

var (
	_ip      string
	_port    string
	_current *paxos.Current

	_connected = true
	_list      = &list.List
	_promises  = make([]paxos.Promise, 0)

	client string
)

type Acceptor struct {
	ip string
}

func Proposer(ip, port string, current *paxos.Current) {
	<-leader.SetAsLeader
	fmt.Println("Paxos: Starting as proposer for the Paxos system")

	_ip = ip
	_port = port
	_current = current
	_current.Leader = true

	go onPromise()
	go onTrust()
}

func onTrust() {
	for _connected {
		ack, ok := <-paxos.TrustChan

		if ok {
			client = ack.IP

			_current.Round++
			_current.Value = ack.Value
			_promises = make([]paxos.Promise, 0)

			fmt.Println("Paxos: Received message from client. Sending preparal to nodes.")
			for i := 0; i < len(_list.NodeList); i++ {
				fmt.Println(_list.NodeList[i].IP)
				tcp.Send(_list.NodeList[i].IP, _port, paxos.Prepare{IP: _ip, RoundNumber: _current.Round})
			}

			pickValue()
		} else {
			_connected = false
		}
	}
}

func onPromise() {
	for _connected {
		ack, ok := <-paxos.PromiseChan

		if ok {
			if ack.RoundNumber == _current.Round {
				_promises = append(_promises, ack)
				fmt.Println("Paxos: Received promise from", ack.IP)
			}
		} else {
			_connected = false
		}
	}
}

func pickValue() {
	delay, _ := time.ParseDuration(strconv.FormatInt((int64)(len(_list.NodeList)*1000/8), 10) + "ms")
	time.Sleep(delay)

	if len(_promises) >= len(_list.NodeList)/2+1 {
		var any = true
		for i := 1; i < len(_promises); i++ {
			if _promises[i-1].LastRoundNumber != _promises[i].LastRoundNumber || _promises[i-1].LastRoundValue != _promises[i].LastRoundValue {
				any = false
				break
			}
		}

		if !any {
			var idx = 0
			for i := 1; i < len(_promises); i++ {
				if _promises[i].LastRoundNumber > _promises[idx].LastRoundNumber {
					idx = i
				}
			}

			_current.Value = _promises[idx].LastRoundValue
		}

		fmt.Println("Paxos: Sending 'Accept' to all nodes with promises for the new value")
		for i := 0; i < len(_promises); i++ {
			tcp.Send(_promises[i].IP, _port, paxos.Accept{IP: _ip, RoundNumber: _current.Round, Value: _current.Value})
		}

		tcp.Send(client, _port, paxos.Accept{IP: _ip, RoundNumber: _current.Round, Value: _current.Value})
		fmt.Println("Paxos: Sent the accepted value to client,", client)
	} else {
		fmt.Println("Paxos: Not enough nodes to write value.")
		tcp.Send(client, _port, paxos.Accept{RoundNumber: _current.Round})
		fmt.Println("Paxos: Sent rejection message to client,", client)
	}
}
