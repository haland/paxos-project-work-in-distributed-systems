package learner

import (
	"fmt"
	"strconv"

	paxos "lab4/controller/paxos/message"
)

var (
	lastRoundNumber int

	_connected bool
	_package   *paxos.PackageLoss
)

func init() {
	_connected = true
}

func Learner() {
	for _connected {
		msg, ok := <-paxos.LearnChan

		if ok {
			if msg.RoundNumber > lastRoundNumber {
				lastRoundNumber = msg.RoundNumber
				fmt.Println("Paxos: Wrote value \"" + msg.Value + "\" on round " + strconv.FormatInt((int64)(msg.RoundNumber), 10))
			}
		} else {
			_connected = false
		}
	}
}
