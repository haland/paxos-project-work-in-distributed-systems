package client

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	leader "lab4/controller/leader/message"
	paxos "lab4/controller/paxos/message"

	"lab4/model/list"
	"lab4/model/network/tcp"
	"lab4/model/network/udp"
)

var (
	_leader  string
	nodeList []list.Node

	wait                 = make(chan int)
	acceptChan           = paxos.AcceptChan
	leaderIPResponseChan = leader.LeaderIPResponseChan
	nodeListResponseChan = leader.NodeListResponseChan
)

func Client(ip, port string) {
	go onAccept()
	fmt.Println("Client: Looking for a Paxos system on the network...")

	for _leader == "" {
		udp.Broadcast("255.255.255.255", port, leader.LEADERIP)
		_leader = onLeaderIPResponse()
		time.Sleep(time.Second)
	}

	fmt.Println("Client: Found a Paxos system. The leader of the system is", _leader)

	for {
		fmt.Println("Client: Getting list of nodes in the Paxos system")
		tcp.Send(_leader, port, leader.NodeListRequest{IP: ip})
		nodeList = onNodeListResponse()

		fmt.Println("Client: Please choose what node to send to using its index number:")
		var aliveNodes = make([]list.Node, 0)
		for i := 0; i < len(nodeList); i++ {
			if nodeList[i].ALIVE {
				aliveNodes = append(aliveNodes, nodeList[i])
			}
		}

		for i := 0; i < len(aliveNodes); i++ {
			fmt.Println("Client:    " + strconv.FormatInt((int64)(i), 10) + ": " + aliveNodes[i].IP)
		}

		fmt.Print("Client: ")
		var key int
		fmt.Scanf("%d", &key)

		if key >= 0 && key < len(aliveNodes) {
			fmt.Println("Client: Please enter a value to send to", aliveNodes[key].IP)
			fmt.Print("Client: ")
			in := bufio.NewReader(os.Stdin)
			msg, _ := in.ReadString('\n')
			msg = strings.Replace(msg, (string)('\n'), "", -1)

			fmt.Println("Client: Sending message to", aliveNodes[key].IP, "with the value:", msg)
			tcp.Send(aliveNodes[key].IP, port, paxos.Received{IP: ip, Value: msg})
			<-wait
		} else {
			fmt.Println("Client: The value entered was out of range.")
		}
	}
}

func onAccept() {
	var connected = true
	for connected {
		msg, ok := <-acceptChan

		if ok {
			if msg.Value != "" {
				fmt.Println("Client: The Paxos system wrote value \"" + msg.Value + "\" on round " + strconv.FormatInt((int64)(msg.RoundNumber), 10))
			} else {
				fmt.Println("Client: The Paxos system could not write value on round " + strconv.FormatInt((int64)(msg.RoundNumber), 10))
			}

			if msg.IP != _leader {
				_leader = msg.IP
			}

			wait <- 1
		} else {
			connected = false
		}
	}
}

func onNodeListResponse() (nodeList []list.Node) {
	resp, ok := <-nodeListResponseChan

	if ok {
		nodeList = resp.NodeList
	}

	return
}

func onLeaderIPResponse() (ip string) {
	var looking = true

	for looking {
		var timeout = make(chan bool, 1)

		go func() {
			time.Sleep(time.Second / 2)
			timeout <- true
		}()

		select {
		case node, ok := <-leaderIPResponseChan:
			looking = false
			if ok {
				ip = node.IP
			}
		case <-timeout:
			looking = false
		}
	}

	return
}
