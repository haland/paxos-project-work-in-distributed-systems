package detector

import (
	"fmt"
	"lab4/controller/failure/message"
	"lab4/model/list"
	"lab4/model/network/tcp"
	"time"
)

var (
	_ip   string
	_port string

	_list                 = &list.List
	heartbeatRequestChan  = message.HeartbeatRequestChan
	heartbeatResponseChan = message.HeartbeatResponseChan
)

func Detector(ip, port string) {
	_ip = ip
	_port = port

	go onHeartbeatRequest()
	go onHeartbeatResponse()

	var (
		SUSPECTED = false
		TIMEOUT   = time.Second / 2
		DELTA     = time.Second / 16
	)

	var connected = true
	for connected {
		if SUSPECTED {
			TIMEOUT = TIMEOUT + DELTA
			fmt.Println("Failure Detector: Increasing the timeout delay for the failure detector.")
		}

		SUSPECTED = false
		time.Sleep(TIMEOUT)

		if !_list.IsEmpty() {
			for i := 0; i < len(_list.NodeList); i++ {
				if _list.NodeList[i].IP != _ip {
					if !_list.NodeList[i].ALIVE && !_list.NodeList[i].SUSPECTED {
						_list.NodeList[i].SUSPECTED = true
					} else if _list.NodeList[i].ALIVE && _list.NodeList[i].SUSPECTED {
						SUSPECTED = true
						_list.NodeList[i].SUSPECTED = false
					}

					if _list.NodeList[i].ALIVE && !_list.NodeList[i].SUSPECTED {
						_list.NodeList[i].SUSPECTED = true
						err := tcp.Send(_list.NodeList[i].IP, _port, message.HeartbeatRequest{IP: _ip})

						if err != nil {
							_list.NodeList[i].ALIVE = false
							fmt.Println("Failure Detector: Node timed out. (", _list.NodeList[i].IP, ")")

							if _list.NodeList[i].LEADER {
								_list.NodeList[i].LEADER = false
								tcp.ExitChan <- 1
							}
						}
					} else {
						tcp.Send(_list.NodeList[i].IP, _port, message.HeartbeatRequest{IP: _ip})
					}
				}
			}
		}
	}
}

func onHeartbeatRequest() {
	var connected = true
	for connected {
		node, ok := <-heartbeatRequestChan

		if ok {
			tcp.Send(node.IP, _port, message.HeartbeatResponse{IP: _ip})
		} else {
			connected = false
		}
	}
}

func onHeartbeatResponse() {
	var connected = true
	for connected {
		node, ok := <-heartbeatResponseChan

		if ok {
			for i := 0; i < len(_list.NodeList); i++ {
				if _list.NodeList[i].IP == node.IP {
					_list.NodeList[i].SUSPECTED = false

					if !_list.NodeList[i].ALIVE {
						_list.NodeList[i].ALIVE = true
						fmt.Println("Failure Detector: Node came back alive. (", node.IP, ")")
					}
					break
				}
			}
		} else {
			connected = false
		}
	}
}
