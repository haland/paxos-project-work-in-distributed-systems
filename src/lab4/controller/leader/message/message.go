package message

import (
	"encoding/gob"
	"lab4/model/list"
)

var (
	LEADERIP      = "LEADERIP"
	LEADERREQUEST = "LEADERREQUEST"

	SetAsLeader       chan int
	LeaderStartedChan chan LeaderStarted

	NodeStartedChan      chan NodeStarted
	LeaderRequestChan    chan LeaderRequest
	LeaderResponseChan   chan LeaderResponse
	NodeListRequestChan  chan NodeListRequest
	NodeListResponseChan chan NodeListResponse
	LeaderIPRequestChan  chan LeaderIPRequest
	LeaderIPResponseChan chan LeaderIPResponse
)

type LeaderIPRequest struct {
	IP string
}

type LeaderIPResponse struct {
	IP string
}

type NodeStarted struct {
	IP string
}

type NodeListRequest struct {
	IP string
}

type NodeListResponse struct {
	IP       string
	NodeList []list.Node
}

type LeaderRequest struct {
	IP string
}

type LeaderResponse struct {
	IP        string
	STARTTIME int64
}

type LeaderStarted struct {
	IP string
}

func init() {
	gob.Register(NodeStarted{})
	gob.Register(NodeListRequest{})
	gob.Register(NodeListResponse{})
	gob.Register(LeaderRequest{})
	gob.Register(LeaderResponse{})
	gob.Register(LeaderStarted{})
	gob.Register(LeaderIPResponse{})

	SetAsLeader = make(chan int)
	LeaderStartedChan = make(chan LeaderStarted)

	NodeStartedChan = make(chan NodeStarted, 5)
	LeaderRequestChan = make(chan LeaderRequest, 5)
	LeaderResponseChan = make(chan LeaderResponse, 5)
	NodeListRequestChan = make(chan NodeListRequest, 5)
	NodeListResponseChan = make(chan NodeListResponse, 5)
	LeaderIPRequestChan = make(chan LeaderIPRequest, 5)
	LeaderIPResponseChan = make(chan LeaderIPResponse, 5)
}
