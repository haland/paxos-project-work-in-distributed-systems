package leader

import (
	leader "lab4/controller/leader/message"

	"lab4/model/list"
	"lab4/model/network/tcp"

	"fmt"
	"time"
)

var (
	_ip   string
	_port string

	_list               = &list.List
	nodeStartedChan     = leader.NodeStartedChan
	leaderRequestChan   = leader.LeaderRequestChan
	leaderResponseChan  = leader.LeaderResponseChan
	nodeListRequestChan = leader.NodeListRequestChan
	leaderIPRequestChan = leader.LeaderIPRequestChan
)

func Leader(ip, port string, startTime int64) {
	_ip = ip
	_port = port

	go onLeaderRequest()
	go onNodeListRequest()
	go onLeaderIPRequest()

	if _list.IsEmpty() {
		startTime = time.Now().Unix()
	}

	_list.Add(list.Node{IP: ip, ALIVE: true, SUSPECTED: false, LEADER: true, STARTTIME: startTime})

	for i := 0; i < len(_list.NodeList); i++ {
		if !_list.NodeList[i].LEADER {
			tcp.Send(_list.NodeList[i].IP, port, leader.LeaderStarted{IP: _ip})
		}
	}

	leader.SetAsLeader <- 1
	<-tcp.ExitChan
}

func onLeaderRequest() {
	var delay = 2 * time.Second
	var connected = true

	for connected {
		node, ok := <-leaderRequestChan

		if node.IP != _ip {
			fmt.Println("Leader Election: Node sent leader request. Sending response. (", node.IP, ")")

			if ok {
				var startTime = time.Now().Unix()
				tcp.Send(node.IP, _port, leader.LeaderResponse{IP: _ip, STARTTIME: startTime})

				fmt.Println("Leader Election: Waiting for node to come online. (", node.IP, ")")

				var timeout = make(chan bool, 1)
				go func() {
					time.Sleep(delay)
					timeout <- true
				}()

				select {
				case _, ok := <-nodeStartedChan:
					if ok {
						fmt.Println("Leader Election: Node is now online.")
						_list.Add(list.Node{IP: node.IP, ALIVE: true, SUSPECTED: false, LEADER: false, STARTTIME: startTime})
					} else {
						connected = false
					}

					for i := 0; i < len(_list.NodeList); i++ {
						if !_list.NodeList[i].LEADER {
							tcp.Send(_list.NodeList[i].IP, _port, leader.NodeListResponse{IP: _ip, NodeList: _list.NodeList})
							fmt.Println("Leader Election: Updated node list sent. (", _list.NodeList[i].IP, ")")
						}
					}
				case <-timeout:
					fmt.Println("Leader Election: Node did not respond in time. Continuing without it. (", node.IP, ")")
				}
			} else {
				connected = false
			}
		}
	}
}

func onNodeListRequest() {
	var connected = true
	for connected {
		node, ok := <-nodeListRequestChan

		if ok {
			tcp.Send(node.IP, _port, leader.NodeListResponse{IP: _ip, NodeList: _list.NodeList})
			fmt.Println("Leader Election: Sent node list (", node.IP, ")")
		} else {
			connected = false
		}
	}
}

func onLeaderIPRequest() {
	var connected = true
	for connected {
		node, ok := <-leaderIPRequestChan
		if ok {
			tcp.Send(node.IP, _port, leader.LeaderIPResponse{_ip})
		} else {
			connected = false
		}
	}
}
