package leaderElection

import (
	"fmt"
	"lab4/controller/leader/leader"
	"lab4/controller/leader/message"
	"lab4/controller/leader/node"
	"lab4/model/list"
	"lab4/model/network/udp"
	"time"
)

var (
	STARTTIME  int64
	LeaderNode list.Node

	_ip   string
	_port string

	_list               = &list.List
	_leaderResponseChan = message.LeaderResponseChan
)

type NoLeaderFound struct {
	Message string
}

func (nlf *NoLeaderFound) Error() string {
	return fmt.Sprintf("at %v, %s",
		nlf.Message)
}

func Election(ip, port string) {
	_ip = ip
	_port = port

	leaderNode, err := electLeader()

	if err == nil {
		fmt.Println("Leader Election: Found existing leader. (", leaderNode.IP, ")\nStarting up as node.")
		LeaderNode = leaderNode
		STARTTIME = leaderNode.STARTTIME
		node.Node(_ip, _port, leaderNode)
	} else {
		fmt.Println("Leader Election: No leader found. Setting self as leader.")
		LeaderNode = list.Node{IP: _ip}
		leader.Leader(_ip, _port, STARTTIME)
	}

	var connected = true
	for connected {
		fmt.Println("Leader Election: The leader is not reachable. Electing new leader now.")

		leaderNode, err := electLeader()
		LeaderNode = leaderNode
		clearLeaderReferences(_list)

		if err == nil {
			if leaderNode.STARTTIME == STARTTIME {
				fmt.Println("Leader Election: This is the new leader.")
				leader.Leader(_ip, _port, STARTTIME)
			} else {
				fmt.Println("Leader Election: The new leader is", leaderNode.IP)
				node.Node(_ip, _port, list.Node{IP: leaderNode.IP})
			}
		}
	}
}

func electLeader() (leader list.Node, err error) {
	if !_list.IsEmpty() {
		leader, err = findLeaderByList()
	} else {
		udp.Broadcast("255.255.255.255", _port, message.LEADERREQUEST)
		leader, err = onLeaderResponse()
	}

	return
}

//Listens on UDP for a response from a potential leader and times out after 
//a given dela if no leader is found in that time.

func onLeaderResponse() (leader list.Node, err error) {
	var connected = true
	var delay = time.Second
	var leaderResponse message.LeaderResponse

	for connected {
		var timeout = make(chan bool, 1)

		go func() {
			time.Sleep(delay)
			timeout <- true
		}()

		select {
		case leaderMessage, ok := <-_leaderResponseChan:
			if ok {
				leaderResponse = leaderMessage
				connected = false
			} else {
				err = &NoLeaderFound{"No leader found"}
				connected = false
			}

			<-timeout
		case <-timeout:
			if connected {
				err = &NoLeaderFound{"No leader found"}
			}

			connected = false
		}

		close(timeout)
	}

	return list.Node{IP: leaderResponse.IP, STARTTIME: leaderResponse.STARTTIME}, err
}

//Finds a leader from the list containing all nodes. The node with the longest 
//running time is chosen. Nodes that allready are leader or are not alive
//should not become the new leader.

func findLeaderByList() (node list.Node, err error) {
	var potentialLeaders = make([]list.Node, 0)
	for i := 0; i < len(_list.NodeList); i++ {
		if !_list.NodeList[i].LEADER && _list.NodeList[i].ALIVE {
			potentialLeaders = append(potentialLeaders, _list.NodeList[i])
		}
	}

	if len(potentialLeaders) <= 0 {
		return list.Node{}, &NoLeaderFound{"No leader found"}
	}

	node = potentialLeaders[0]
	for i := 0; i < len(potentialLeaders); i++ {
		if potentialLeaders[i].STARTTIME < node.STARTTIME {
			node = potentialLeaders[i]
		}
	}

	return node, nil
}

//Clears the boolean variable for leader for every node in the node list. No
//node is supposed to be a leader when the leader election takes place.

func clearLeaderReferences(_list *list.NodeList) {
	for i := 0; i < len(_list.NodeList); i++ {
		if _list.NodeList[i].LEADER {
			_list.NodeList[i].LEADER = false
		}
	}
}
