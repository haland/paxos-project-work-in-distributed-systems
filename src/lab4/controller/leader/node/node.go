package node

import (
	leader "lab4/controller/leader/message"

	"lab4/model/list"
	"lab4/model/network/tcp"

	"fmt"
)

var (
	_ip        string
	_port      string
	_connected bool
	_leader    list.Node

	_list                = &list.List
	leaderRequestChan    = leader.LeaderRequestChan
	nodeListRequestChan  = leader.NodeListRequestChan
	nodeListResponseChan = leader.NodeListResponseChan
)

func Node(ip, port string, leaderNode list.Node) {
	_ip = ip
	_port = port
	_connected = true
	_leader = leaderNode

	go onLeaderRequest()
	go onNodeListRequest()
	go onNodeListResponse()

	if _list.IsEmpty() {
		tcp.Send(_leader.IP, port, leader.NodeStarted{IP: _ip})
		fmt.Println("Leader Election: Sent alive message to leader. (", _leader.IP, ")")
	} else {
		<-leader.LeaderStartedChan
		tcp.Send(_leader.IP, port, leader.NodeListRequest{IP: _ip})
		fmt.Println("Leader Election: Sent node list request (", _leader.IP, ")")
	}

	<-tcp.ExitChan

	_connected = false
	leaderRequestChan <- leader.LeaderRequest{}
	nodeListRequestChan <- leader.NodeListRequest{}
	nodeListResponseChan <- leader.NodeListResponse{}
}

func onLeaderRequest() {
	for _connected {
		_, ok := <-leaderRequestChan
		if !ok {
			_connected = false
		}
	}
}

func onNodeListRequest() {
	for _connected {
		_, ok := <-nodeListRequestChan
		if !ok {
			_connected = false
		}
	}
}

func onNodeListResponse() {
	for _connected {
		response, ok := <-nodeListResponseChan

		if ok {
			if response.IP != "" {
				fmt.Println("Leader Election: Received new node list. (", response.IP, ")")
				for i := 0; i < len(response.NodeList); i++ {
					_list.Add(response.NodeList[i])
				}
			}
		} else {
			_connected = false
		}
	}
}
