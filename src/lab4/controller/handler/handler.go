package messageHandler

import (
	failure "lab4/controller/failure/message"
	leader "lab4/controller/leader/message"
	paxos "lab4/controller/paxos/message"

	"lab4/model/network/tcp"
	"lab4/model/network/udp"

	"strings"
)

func HandleTCPMessages() {
	var connected = true
	for connected {
		tcpMessage, ok := <-tcp.TCPChan

		if ok {
			switch tcpMessage.Message.(type) {
			case failure.HeartbeatRequest:
				failure.HeartbeatRequestChan <- tcpMessage.Message.(failure.HeartbeatRequest)
			case failure.HeartbeatResponse:
				failure.HeartbeatResponseChan <- tcpMessage.Message.(failure.HeartbeatResponse)
			case leader.NodeListRequest:
				leader.NodeListRequestChan <- tcpMessage.Message.(leader.NodeListRequest)
			case leader.NodeListResponse:
				leader.NodeListResponseChan <- tcpMessage.Message.(leader.NodeListResponse)
			case leader.NodeStarted:
				leader.NodeStartedChan <- tcpMessage.Message.(leader.NodeStarted)
			case leader.LeaderResponse:
				leader.LeaderResponseChan <- tcpMessage.Message.(leader.LeaderResponse)
			case leader.LeaderStarted:
				leader.LeaderStartedChan <- tcpMessage.Message.(leader.LeaderStarted)
			case paxos.Learn:
				paxos.LearnChan <- tcpMessage.Message.(paxos.Learn)
			case paxos.Trust:
				paxos.TrustChan <- tcpMessage.Message.(paxos.Trust)
			case paxos.Accept:
				paxos.AcceptChan <- tcpMessage.Message.(paxos.Accept)
			case paxos.Prepare:
				paxos.PrepareChan <- tcpMessage.Message.(paxos.Prepare)
			case paxos.Promise:
				paxos.PromiseChan <- tcpMessage.Message.(paxos.Promise)
			case paxos.Received:
				paxos.ReceivedChan <- tcpMessage.Message.(paxos.Received)
			case leader.LeaderIPResponse:
				leader.LeaderIPResponseChan <- tcpMessage.Message.(leader.LeaderIPResponse)
			}
		} else {
			connected = false
		}
	}

	close(paxos.TrustChan)
	close(paxos.AcceptChan)
	close(paxos.PromiseChan)
	close(paxos.PrepareChan)
	close(paxos.ReceivedChan)

	close(failure.HeartbeatRequestChan)
	close(failure.HeartbeatResponseChan)

	close(leader.NodeStartedChan)
	close(leader.LeaderStartedChan)
	close(leader.LeaderResponseChan)
	close(leader.NodeListRequestChan)
	close(leader.NodeListResponseChan)
	close(leader.LeaderIPResponseChan)
}

func HandleUDPMessages() {
	var connected = true
	for connected {
		udpMessage, ok := <-udp.UDPChan

		if ok {
			if strings.Contains(udpMessage.Message, leader.LEADERREQUEST) {
				leader.LeaderRequestChan <- leader.LeaderRequest{udpMessage.IP}
			} else if strings.Contains(udpMessage.Message, leader.LEADERIP) {
				leader.LeaderIPRequestChan <- leader.LeaderIPRequest{udpMessage.IP}
			}
		} else {
			connected = false
		}
	}

	close(leader.LeaderRequestChan)
	close(leader.LeaderIPRequestChan)
}
