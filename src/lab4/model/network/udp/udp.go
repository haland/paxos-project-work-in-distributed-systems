package udp

import (
	"net"
	"strings"
	"time"
)

var (
	UDPChan   chan UDPMessage
	ExitChan  chan int
	connected bool
)

type UDPMessage struct {
	IP      string
	Message string
}

func init() {
	UDPChan = make(chan UDPMessage, 5)
	ExitChan = make(chan int, 1)
	connected = true
}

func UDPSocket(port string) {
	udpAddr, err1 := net.ResolveUDPAddr("udp4", strings.Join([]string{":", port}, ""))

	if err1 == nil {
		listener, err2 := net.ListenUDP("udp4", udpAddr)

		if err2 == nil {
			for connected {
				data := make([]byte, 4096)
				listener.SetDeadline(time.Now().Add(time.Second / 8))

				n, addr, err3 := listener.ReadFromUDP(data[0:])

				if err3 == nil {
					connected = send(UDPChan, UDPMessage{addr.IP.String(), string(data[0:n])})
				} else {
					connected = send(UDPChan, UDPMessage{})
				}
			}
		}

		listener.Close()
	}

	ExitChan <- 1
}

func Broadcast(ip, port, message string) {
	udpAddr, err1 := net.ResolveUDPAddr("udp", strings.Join([]string{ip, ":", port}, ""))

	if err1 == nil {
		conn, err2 := net.DialUDP("udp", nil, udpAddr)

		if err2 == nil {
			conn.Write([]byte(message))
		}
	}
}

func send(c chan UDPMessage, t UDPMessage) bool {
	defer func() { recover() }()
	c <- t
	return true
}
