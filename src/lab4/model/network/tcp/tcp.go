package tcp

import (
	"encoding/gob"
	"net"
	"strings"
	"time"
)

var (
	TCPChan   chan TCPMessage
	ExitChan  chan int
	connected bool
)

type TCPMessage struct {
	IP      string
	Message interface{}
}

func init() {
	gob.Register(TCPMessage{})

	TCPChan = make(chan TCPMessage, 5)
	ExitChan = make(chan int, 1)
	connected = true
}

func TCPSocket(port string) {
	tcpAddr, err1 := net.ResolveTCPAddr("tcp", strings.Join([]string{":", port}, ""))

	if err1 == nil {
		listener, err2 := net.ListenTCP("tcp", tcpAddr)

		if err2 == nil {
			for connected {
				listener.SetDeadline(time.Now().Add(time.Second / 8))

				conn, err3 := listener.Accept()

				if err3 == nil {
					decoder := gob.NewDecoder(conn)

					var msg interface{}
					err4 := decoder.Decode(&msg)

					if err4 == nil {
						addr := conn.RemoteAddr().String()
						connected = send(TCPChan, TCPMessage{addr[0:strings.Index(addr, ":")], msg})
					}

					conn.Close()
				} else {
					connected = send(TCPChan, TCPMessage{})
				}
			}
		}

		listener.Close()
	}

	ExitChan <- 1
}

func Send(ip string, port string, msg interface{}) (err error) {
	conn, err := net.Dial("tcp", strings.Join([]string{ip, ":", port}, ""))

	if err == nil {
		encoder := gob.NewEncoder(conn)
		encoder.Encode(&msg)
	}

	return
}

func send(c chan TCPMessage, t TCPMessage) bool {
	defer func() { recover() }()
	c <- t
	return true
}
