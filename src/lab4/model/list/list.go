package list

var List NodeList

type Node struct {
	IP                       string
	STARTTIME                int64
	ALIVE, SUSPECTED, LEADER bool
}

type NodeList struct {
	NodeList []Node
}

func init() {
	if List.NodeList == nil {
		List = NodeList{make([]Node, 0)}
	}
}

func (n *NodeList) Add(node Node) {
	for i := 0; i < len(n.NodeList); i++ {
		if n.NodeList[i].IP == node.IP {
			n.NodeList[i].LEADER = node.LEADER
			n.NodeList[i].STARTTIME = node.STARTTIME

			return
		}
	}

	n.NodeList = append(n.NodeList, node)
}

func (n *NodeList) Contains(node Node) bool {
	for i := 0; i < len(n.NodeList); i++ {
		if n.NodeList[i].IP == node.IP {
			return true
		}
	}

	return false
}

func (n *NodeList) IsEmpty() bool {
	return len(n.NodeList) <= 0
}
