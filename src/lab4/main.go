package main

import (
	handler "lab4/controller/handler"
	"lab4/controller/paxos/client"
	"lab4/controller/paxos/server"

	"lab4/model/network/tcp"
	"lab4/model/network/udp"

	"fmt"
	"net"
	"strings"
)

var (
	exit = make(chan bool, 1)
)

type IPAddressNotFound struct {
	Message string
}

func (e *IPAddressNotFound) Error() string {
	return fmt.Sprintf("at %v, %s",
		e.Message)
}

func main() {
	port := readPort()
	ip, err := getLocalIPAddress()

	if err == nil {
		go tcp.TCPSocket(port)
		go udp.UDPSocket(port)
		go handler.HandleTCPMessages()
		go handler.HandleUDPMessages()

		fmt.Println("Paxos: Select if the machine is a client or apart of Paxos:")
		fmt.Println("Paxos:    1: Client")
		fmt.Println("Paxos:    2: Server")

		var started = false
		for !started {
			var key string
			fmt.Print("Paxos: ")
			fmt.Scanf("%s", &key)

			if key == "1" {
				started = true
				go client.Client(ip, port)
			} else if key == "2" {
				started = true
				go server.Server(ip, port)
			} else {
				fmt.Println("Paxos: Illegal argument. Only the numbers 1 and 2 may be entered.")
			}
		}
		<-exit
	} else {
		fmt.Println("System: Could not get local IP address. Exiting program")
	}
}

func readPort() (port string) {
	fmt.Println("System: Please enter port for the distributed system to work on")
	fmt.Print("System: ")
	fmt.Scanf("%s", &port)
	return
}

func getLocalIPAddress() (string, error) {
	var addrs, _ = net.InterfaceAddrs()
	for i := 0; i < len(addrs); i++ {
		if strings.Contains(addrs[i].String(), "/24") {
			return strings.Replace(addrs[i].String(), "/24", "", 1), nil
		}
	}

	return "", &IPAddressNotFound{"IP Address not found"}
}
