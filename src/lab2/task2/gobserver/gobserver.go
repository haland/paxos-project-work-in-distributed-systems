//use: sortMsgs [file]
package main

import (
	"encoding/gob"
	"fmt"
	"io"
	"lab2/task2/messages"
	"net"
	"os"
	"time"
)

var (
	semMsg  = make(chan int)
	semErr  = make(chan int)
	inChan  = make(chan interface{}, 5)     //channel from demarshaler to sort	
	msgChan = make(chan messages.StrMsg, 5) //message channel
	errChan = make(chan messages.ErrMsg, 5) //error channel
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Wrong argument.\n Usage: gobclient server:port")
		return
	}

	go handleMsgs(msgChan)
	go handleErrors(errChan)
	go sort(inChan, msgChan, errChan)
	demarshal(os.Args[1], inChan)
	<-semMsg
	<-semErr
}

func demarshal(service string, inChan chan interface{}) {
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)

	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)

	conn, err := listener.Accept()
	checkError(err)

	conn.SetDeadline(time.Now().Add(30 * time.Second))

	for {
		decoder := gob.NewDecoder(conn)

		var msg interface{}

		err := decoder.Decode(&msg)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				checkError(err)
			}
		}

		inChan <- msg
		conn.SetDeadline(time.Now().Add(30 * time.Second))
	}

	close(inChan)
}

func handleMsgs(inchan chan messages.StrMsg) {
	senders := ""

	for {
		msg, ok := <-inchan

		if ok {
			senders = senders + msg.Sender + ", "
			fmt.Println("Message received from", msg.Sender, ": ", msg.Content)
		} else {
			if senders != "" {
				fmt.Println("The senders: " + senders)
			}
			break
		}
	}

	semMsg <- 1
}

func handleErrors(inchan chan messages.ErrMsg) {
	errors := ""

	for {
		msg, ok := <-inchan

		if ok {
			errors = errors + msg.Error + ", "
			fmt.Println("Error received from", msg.Sender, ": ", msg.Error)
		} else {
			if errors != "" {
				fmt.Println("The errors: " + errors)
			}
			break
		}
	}

	semErr <- 1
}

func sort(inChan chan interface{}, msgChan chan messages.StrMsg, errChan chan messages.ErrMsg) {
	for {
		m, ok := <-inChan

		if ok {
			switch m.(type) {
			case messages.StrMsg:
				msgChan <- m.(messages.StrMsg)
			case messages.ErrMsg:
				errChan <- m.(messages.ErrMsg)
			case messages.PingMsg:
				fmt.Println("Received 'keep alive' message")
			}
		} else {
			break
		}
	}

	close(errChan)
	close(msgChan)
}

func checkError(e error) {
	if e != nil {
		fmt.Println("Error:", e)
	}
}
