// SimpleEchoServer
package main

import (
	"fmt"
	"net"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "Usage: echocl host:port\n")
		os.Exit(1)
	}

	service := os.Args[1]
	udpAddr, err := net.ResolveUDPAddr("udp4", service)
	checkError(err)
	conn, err := net.ListenUDP("udp", udpAddr)
	checkError(err)

	handleClient(conn)
}

func handleClient(conn net.Conn) {
	var buf [512]byte
	for {
		_, err := conn.Read(buf[0:])
		if err != nil {
			return
		}

		fmt.Println(string(buf[0:]))
		//_, err2 := conn.Write(buf[0:n])

		//if err2 != nil {
		//	fmt.Println("shit just got real!")
		//	return
		//}
	}

	conn.Close() // we're finished
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
