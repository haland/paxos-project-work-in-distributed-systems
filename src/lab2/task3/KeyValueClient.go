package main

import (
	"fmt"
	"net/rpc"
)

type Pair struct {
	Key, Value string
}

type Found struct {
	Value string
	Ok    bool
}

func Client() *rpc.Client {
	c, err := rpc.Dial("tcp", "127.0.0.1:12110")

	if err != nil {
		fmt.Println(err)
	}

	return c
}

func InsertValue(client *rpc.Client, key string, value string) {
	args := Pair{key, value}

	var reply bool
	err := client.Call("KeyValue.InsertNew", args, &reply)

	if err != nil {
		fmt.Println("Error inserting value")
	} else {
		fmt.Println("Inserting finished")
	}
}

func CheckInsertedValue(client *rpc.Client, key string) {
	var lookup Found
	err := client.Call("KeyValue.LookUp", key, &lookup)

	if err != nil {
		fmt.Println("Error checking value")
	} else {
		if lookup.Ok {
			fmt.Println("Value found:", lookup.Value)
		} else {
			fmt.Println("Value not found for key:", key)
		}

	}
}

func main() {
	var Client = Client()
	var running = true

	for running == true {
		fmt.Println("1 to insert value, 2 to check value, 3 to exit")
		var choice int
		fmt.Scanf("%d", &choice)

		switch choice {
		case 1:
			fmt.Println("Enter key:")
			var key string
			fmt.Scanf("%s", &key)
			fmt.Println("Enter value:")
			var value string
			fmt.Scanf("%s", &value)
			InsertValue(Client, key, value)
		case 2:
			fmt.Println("Enter key:")
			var value string
			fmt.Scanf("%s", &value)
			CheckInsertedValue(Client, value)
		case 3:
			running = false
		}
	}
}
