package proposer

import (
	paxos "lab6/controller/paxos/message"

	"lab6/controller/leader/leader"
	"lab6/controller/leader/message"
	"lab6/model/network/tcp"
	"lab6/model/nodelist"
	"lab6/model/slotlist"

	"fmt"
	"strconv"
	"time"
)

var (
	promises []paxos.Promise

	quorumChan            = make(chan int, 0)
	hasQuarum             = false
	connected             = false
	currentRoundNumber    = 0
	currentSequenceNumber = 0
)

func Proposer(ip, port string, tcpSocket *tcp.TCPSocket, nodeList *nodelist.NodeList, slotList *slotlist.SlotList) {
	<-message.SetAsLeader
	fmt.Println("Paxos: Starting as proposer for the Paxos system")

	go onPromise(ip, port, nodeList)
	go onSlotRequest(ip, port, tcpSocket, slotList)

	quorum(ip, port, tcpSocket, nodeList)

	var any = true
	for i := 0; i < len(promises); i++ {
		if promises[i].LastRoundNumber != 0 {
			any = false
			break
		}
	}

	if !any {
		var idx = 0
		for i := 1; i < len(promises); i++ {
			if promises[i].LastRoundNumber > promises[idx].LastRoundNumber {
				idx = i
			}
		}

		for i := 0; i < len(nodeList.List); i++ {
			tcpSocket.Send(nodeList.List[i].IP, port, paxos.Accept{IP: ip, SequenceNumber: currentSequenceNumber, RoundNumber: currentRoundNumber, RoundValue: promises[idx].LastRoundValue})
		}
	}

	unfilled := slotList.UnfilledSlots()
	for i := 0; i < len(unfilled); i++ {
		for j := 0; j < len(nodeList.List); j++ {
			tcpSocket.Send(nodeList.List[j].IP, port, paxos.Accept{IP: ip, SequenceNumber: unfilled[i], RoundNumber: currentRoundNumber})
		}
	}

	var connected = true
	for connected {
		select {
		case <-leader.NewNodeChan:
			quorum(ip, port, tcpSocket, nodeList)
		case msg, ok := <-paxos.TrustChan:
			if ok {
				currentSequenceNumber = currentSequenceNumber + 1
				go sendAccept(ip, port, tcpSocket, currentSequenceNumber, msg, nodeList)
			} else {
				connected = false
			}
		}

	}
}

func quorum(ip, port string, tcpSocket *tcp.TCPSocket, nodeList *nodelist.NodeList) {
	hasQuarum = false
	promises = make([]paxos.Promise, 0)
	currentRoundNumber = currentRoundNumber + 1

	var isQuarum = false
	for !isQuarum {
		fmt.Println("Paxos: Sending prepare for a new round.")
		for i := 0; i < len(nodeList.List); i++ {
			tcpSocket.Send(nodeList.List[i].IP, port, paxos.Prepare{IP: ip, RoundNumber: currentRoundNumber})
		}

		timeout := make(chan int, 0)
		delay, _ := time.ParseDuration(strconv.FormatInt((int64)(len(nodeList.List)*1000/8), 10) + "ms")

		go func() {
			time.Sleep(delay)
			timeout <- 1
		}()

		select {
		case <-quorumChan:
			isQuarum = true
		case <-timeout:
			currentRoundNumber = currentRoundNumber + 1
			fmt.Println("Paxos: Not enough nodes to meet quorum. Increasing round number and trying again.")
		}
	}

	fmt.Println("Paxos: Quorum reached. Starting up round.")
}

func sendAccept(ip, port string, tcpSocket *tcp.TCPSocket, sequenceNumber int, msg paxos.Trust, nodeList *nodelist.NodeList) {
	for i := 0; i < len(nodeList.List); i++ {
		tcpSocket.Send(nodeList.List[i].IP, port, paxos.Accept{IP: ip, ClientIP: msg.IP, SequenceNumber: sequenceNumber, RoundNumber: currentRoundNumber, RoundValue: msg.Value})
	}
}

func onPromise(ip, port string, nodeList *nodelist.NodeList) {
	var connected = true
	for connected {
		msg, ok := <-paxos.PromiseChan

		if ok {
			if msg.RoundNumber == currentRoundNumber {
				promises = append(promises, msg)
				fmt.Println("Paxos: Received promise from", msg.IP)

				if msg.LastSequenceNumber > currentSequenceNumber {
					currentSequenceNumber = msg.LastSequenceNumber
				}

				if !hasQuarum && (len(promises) >= len(nodeList.List)/2+1) {
					hasQuarum = true
					quorumChan <- 1
				}
			}
		} else {
			connected = false
		}
	}
}

func onSlotRequest(ip, port string, tcpSocket *tcp.TCPSocket, slotList *slotlist.SlotList) {
	var connected = true
	for connected {
		msg := <-paxos.SlotRequestChan
		clientIP, value := slotList.Pull()
		tcpSocket.Send(msg.IP, port, paxos.SlotResponse{IP: clientIP, Message: value})
	}
}
