package message

import (
	"encoding/gob"
	application "lab6/controller/application"
)

var (
	LearnChan        = make(chan Learn, 10)
	TrustChan        = make(chan Trust, 10)
	AcceptChan       = make(chan Accept, 10)
	PrepareChan      = make(chan Prepare, 10)
	PromiseChan      = make(chan Promise, 10)
	ReceivedChan     = make(chan Received, 10)
	SlotRequestChan  = make(chan SlotRequest, 10)
	SlotResponseChan = make(chan SlotResponse, 10)
)

func init() {
	gob.Register(SlotResponse{})
	gob.Register(SlotRequest{})
	gob.Register(Received{})
	gob.Register(Prepare{})
	gob.Register(Promise{})
	gob.Register(Accept{})
	gob.Register(Trust{})
	gob.Register(Learn{})

	gob.Register(application.Deposit{})
	gob.Register(application.Withdraw{})
	gob.Register(application.Transfer{})
	gob.Register(application.Balance{})
}

type PackageLoss struct {
	Lost bool
}

type SlotRequest struct {
	IP string
}

type SlotResponse struct {
	IP      string
	Message interface{}
}

type Received struct {
	IP    string
	Value interface{}
}

type Learn struct {
	ClientIP       string
	SequenceNumber int
	RoundNumber    int
	RoundValue     interface{}
}

type Trust struct {
	IP    string
	Value interface{}
}

type Prepare struct {
	IP          string
	RoundNumber int
}

type Promise struct {
	IP                 string
	RoundNumber        int
	LastRoundNumber    int
	LastRoundValue     interface{}
	LastSequenceNumber int
}

type Accept struct {
	IP             string
	ClientIP       string
	SequenceNumber int
	RoundNumber    int
	RoundValue     interface{}
}
