package handler

import (
	failure "lab6/controller/failure/message"
	leader "lab6/controller/leader/message"
	paxos "lab6/controller/paxos/message"

	"lab6/model/network/tcp"
	"lab6/model/network/udp"

	"strings"
)

func HandleTCPMessages(tcpSocket *tcp.TCPSocket) {
	var connected = true
	for connected {
		tcpMessage, ok := <-tcpSocket.Chan()

		if ok {
			switch msg := tcpMessage.Message.(type) {
			case paxos.Learn:
				paxos.LearnChan <- msg
			case paxos.Trust:
				paxos.TrustChan <- msg
			case paxos.Accept:
				paxos.AcceptChan <- msg
			case paxos.Prepare:
				paxos.PrepareChan <- msg
			case paxos.Promise:
				paxos.PromiseChan <- msg
			case paxos.Received:
				paxos.ReceivedChan <- msg
			case paxos.SlotRequest:
				paxos.SlotRequestChan <- msg
			case paxos.SlotResponse:
				paxos.SlotResponseChan <- msg

			case failure.HeartbeatRequest:
				failure.HeartbeatRequestChan <- msg
			case failure.HeartbeatResponse:
				failure.HeartbeatResponseChan <- msg

			case leader.NodeStarted:
				leader.NodeStartedChan <- msg
			case leader.LeaderStarted:
				leader.LeaderStartedChan <- msg
			case leader.LeaderResponse:
				leader.LeaderResponseChan <- msg
			case leader.NodeListRequest:
				leader.NodeListRequestChan <- msg
			case leader.NodeListResponse:
				leader.NodeListResponseChan <- msg
			case leader.LeaderIPResponse:
				leader.LeaderIPResponseChan <- msg
			}
		} else {
			connected = false
		}
	}

	close(paxos.TrustChan)
	close(paxos.AcceptChan)
	close(paxos.PromiseChan)
	close(paxos.PrepareChan)
	close(paxos.ReceivedChan)
	close(paxos.SlotRequestChan)
	close(paxos.SlotResponseChan)

	close(failure.HeartbeatRequestChan)
	close(failure.HeartbeatResponseChan)

	close(leader.NodeStartedChan)
	close(leader.LeaderStartedChan)
	close(leader.LeaderResponseChan)
	close(leader.NodeListRequestChan)
	close(leader.NodeListResponseChan)
	close(leader.LeaderIPResponseChan)
}

func HandleUDPMessages() {
	var connected = true
	for connected {
		udpMessage, ok := <-udp.UDPChan

		if ok {
			if strings.Contains(udpMessage.Message, leader.LEADERREQUEST) {
				leader.LeaderRequestChan <- leader.LeaderRequest{udpMessage.IP}
			} else if strings.Contains(udpMessage.Message, leader.LEADERIP) {
				leader.LeaderIPRequestChan <- leader.LeaderIPRequest{udpMessage.IP}
			}
		} else {
			connected = false
		}
	}

	close(leader.LeaderRequestChan)
	close(leader.LeaderIPRequestChan)
}
