package leader

import (
	"lab6/controller/leader/message"
	"lab6/model/network/tcp"
	"lab6/model/nodelist"

	"fmt"
	"time"
)

var NewNodeChan = make(chan bool, 10)

func Leader(ip, port string, startTime int64, tcpSocket *tcp.TCPSocket, nodeList *nodelist.NodeList, leaderDroppedChan chan int) {
	if nodeList.IsEmpty() {
		startTime = time.Now().Unix()
	}

	nodeList.Add(nodelist.Node{IP: ip, IsAlive: true, IsSuspected: false, IsLeader: true, Starttime: startTime})

	go onLeaderRequest(ip, port, startTime, tcpSocket, nodeList)
	go onNodeListRequest(ip, port, tcpSocket, nodeList)
	go onLeaderIPRequest(ip, port, tcpSocket)

	for i := 0; i < len(nodeList.List); i++ {
		if !nodeList.List[i].IsLeader {
			tcpSocket.Send(nodeList.List[i].IP, port, message.LeaderStarted{IP: ip})
		}
	}

	message.SetAsLeader <- 1
	<-leaderDroppedChan
}

func onLeaderRequest(ip, port string, startTime int64, tcpSocket *tcp.TCPSocket, nodeList *nodelist.NodeList) {
	var delay = 2 * time.Second
	var connected = true

	for connected {
		node, ok := <-message.LeaderRequestChan

		if node.IP != ip {
			fmt.Println("Leader Election: Node sent leader request. Sending response. (", node.IP, ")")

			if ok {
				var startTime = time.Now().Unix()
				tcpSocket.Send(node.IP, port, message.LeaderResponse{IP: ip, STARTTIME: startTime})

				fmt.Println("Leader Election: Waiting for node to come online. (", node.IP, ")")

				var timeout = make(chan bool, 1)
				go func() {
					time.Sleep(delay)
					timeout <- true
				}()

				select {
				case on, ok := <-message.NodeStartedChan:
					if ok && on.IP == node.IP {
						fmt.Println("Leader Election: Node is now online.")
						nodeList.Add(nodelist.Node{IP: node.IP, IsAlive: true, IsSuspected: false, IsLeader: false, Starttime: startTime})
						NewNodeChan <- true
					} else {
						connected = false
					}

					for i := 0; i < len(nodeList.List); i++ {
						if !nodeList.List[i].IsLeader {
							tcpSocket.Send(nodeList.List[i].IP, port, message.NodeListResponse{IP: ip, NodeList: nodeList.List})
							fmt.Println("Leader Election: Updated node list sent. (", nodeList.List[i].IP, ")")
						}
					}
				case <-timeout:
					fmt.Println("Leader Election: Node did not respond in time. Continuing without it. (", node.IP, ")")
				}
			} else {
				connected = false
			}
		}
	}
}

func onNodeListRequest(ip, port string, tcpSocket *tcp.TCPSocket, nodeList *nodelist.NodeList) {
	var connected = true
	for connected {
		node, ok := <-message.NodeListRequestChan

		if ok {
			tcpSocket.Send(node.IP, port, message.NodeListResponse{IP: ip, NodeList: nodeList.List})
			fmt.Println("Leader Election: Sent node list (", node.IP, ")")
		} else {
			connected = false
		}
	}
}

func onLeaderIPRequest(ip, port string, tcpSocket *tcp.TCPSocket) {
	var connected = true
	for connected {
		node, ok := <-message.LeaderIPRequestChan
		if ok {
			tcpSocket.Send(node.IP, port, message.LeaderIPResponse{ip})
		} else {
			connected = false
		}
	}
}
