package node

import (
	"lab6/controller/leader/message"
	"lab6/model/network/tcp"
	"lab6/model/nodelist"

	"fmt"
)

var (
	connected bool
)

func Node(ip, port string, tcpSocket *tcp.TCPSocket, leader nodelist.Node, nodeList *nodelist.NodeList, leaderDroppedChan chan int) {
	connected = true

	go onLeaderRequest()
	go onNodeListRequest()
	go onNodeListResponse(nodeList)

	if nodeList.IsEmpty() {
		tcpSocket.Send(leader.IP, port, message.NodeStarted{IP: ip})
		fmt.Println("Leader Election: Sent alive message to leader. (", leader.IP, ")")
	} else {
		<-message.LeaderStartedChan
		tcpSocket.Send(leader.IP, port, message.NodeListRequest{IP: ip})
		fmt.Println("Leader Election: Sent node list request (", leader.IP, ")")
	}

	<-leaderDroppedChan

	connected = false
	message.LeaderRequestChan <- message.LeaderRequest{}
	message.NodeListRequestChan <- message.NodeListRequest{}
	message.NodeListResponseChan <- message.NodeListResponse{}
}

func onLeaderRequest() {
	for connected {
		_, ok := <-message.LeaderRequestChan

		if !ok {
			connected = false
		}
	}
}

func onNodeListRequest() {
	for connected {
		_, ok := <-message.NodeListRequestChan

		if !ok {
			connected = false
		}
	}
}

func onNodeListResponse(nodeList *nodelist.NodeList) {
	for connected {
		msg, ok := <-message.NodeListResponseChan

		if ok {
			if msg.IP != "" {
				fmt.Println("Leader Election: Received new node list. (", msg.IP, ")")
				for i := 0; i < len(msg.NodeList); i++ {
					nodeList.Add(msg.NodeList[i])
				}
			}
		} else {
			connected = false
		}
	}
}
