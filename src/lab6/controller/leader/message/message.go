package message

import (
	"encoding/gob"
	"lab6/model/nodelist"
	"net"
)

var (
	LEADERIP      = "LEADERIP"
	LEADERREQUEST = "LEADERREQUEST"

	SetAsLeader       = make(chan int)
	LeaderStartedChan = make(chan LeaderStarted)

	NodeStartedChan      = make(chan NodeStarted, 10)
	LeaderRequestChan    = make(chan LeaderRequest, 10)
	LeaderResponseChan   = make(chan LeaderResponse, 10)
	NodeListRequestChan  = make(chan NodeListRequest, 10)
	NodeListResponseChan = make(chan NodeListResponse, 10)
	LeaderIPRequestChan  = make(chan LeaderIPRequest, 10)
	LeaderIPResponseChan = make(chan LeaderIPResponse, 10)
)

type LeaderIPRequest struct {
	IP string
}

type LeaderIPResponse struct {
	IP string
}

type NodeStarted struct {
	IP   string
	Conn net.Conn
}

type NodeListRequest struct {
	IP string
}

type NodeListResponse struct {
	IP       string
	NodeList []nodelist.Node
}

type LeaderRequest struct {
	IP string
}

type LeaderResponse struct {
	IP        string
	STARTTIME int64
}

type LeaderStarted struct {
	IP string
}

func init() {
	gob.Register(NodeStarted{})
	gob.Register(NodeListRequest{})
	gob.Register(NodeListResponse{})
	gob.Register(LeaderRequest{})
	gob.Register(LeaderResponse{})
	gob.Register(LeaderStarted{})
	gob.Register(LeaderIPResponse{})
}
