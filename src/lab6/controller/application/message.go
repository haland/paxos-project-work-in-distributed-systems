package message

type Deposit struct {
	Amount        int
	AccountNumber string
}

type Withdraw struct {
	Amount        int
	AccountNumber string
}

type Transfer struct {
	Amount      int
	ToAccount   string
	FromAccount string
}

type Balance struct {
	AccountNumber string
}
