package message

import (
	"encoding/gob"
)

var (
	HeartbeatRequestChan  chan HeartbeatRequest
	HeartbeatResponseChan chan HeartbeatResponse
)

type HeartbeatRequest struct {
	IP string
}

type HeartbeatResponse struct {
	IP string
}

func init() {
	gob.Register(HeartbeatRequest{})
	gob.Register(HeartbeatResponse{})

	HeartbeatRequestChan = make(chan HeartbeatRequest, 10)
	HeartbeatResponseChan = make(chan HeartbeatResponse, 10)
}
