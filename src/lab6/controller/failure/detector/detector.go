package detector

import (
	"fmt"
	"lab6/controller/failure/message"
	"lab6/model/network/tcp"
	"lab6/model/nodelist"
	"time"
)

var (
	heartbeatRequestChan  = message.HeartbeatRequestChan
	heartbeatResponseChan = message.HeartbeatResponseChan
)

func Detector(ip, port string, tcpSocket *tcp.TCPSocket, nodeList *nodelist.NodeList, leaderDroppedChan chan int) {
	go onHeartbeatRequest(ip, port, tcpSocket)
	go onHeartbeatResponse(ip, port, nodeList)

	var (
		//SUSPECTED = false
		TIMEOUT = time.Second / 2
		//DELTA     = time.Second / 16
	)

	var connected = true
	for connected {
		//if SUSPECTED {
		//	TIMEOUT = TIMEOUT + DELTA
		//	fmt.Println("Failure Detector: Increasing the timeout delay for the failure detector.")
		//}

		//SUSPECTED = false
		time.Sleep(TIMEOUT)

		if !nodeList.IsEmpty() {
			for i := 0; i < len(nodeList.List); i++ {
				if nodeList.List[i].IP != ip {
					if nodeList.List[i].IsAlive && nodeList.List[i].IsSuspected {
						//SUSPECTED = true
						nodeList.List[i].IsSuspected = false
					} else if !nodeList.List[i].IsAlive && !nodeList.List[i].IsSuspected {
						nodeList.List[i].IsSuspected = true
					}

					if nodeList.List[i].IsAlive && !nodeList.List[i].IsSuspected {
						nodeList.List[i].IsSuspected = true
						err := tcpSocket.Send(nodeList.List[i].IP, port, message.HeartbeatRequest{IP: ip})

						if err != nil {
							nodeList.List[i].IsAlive = false
							fmt.Println("Failure Detector: Node timed out. (", nodeList.List[i].IP, ")")

							if nodeList.List[i].IsLeader {
								nodeList.List[i].IsLeader = false
								leaderDroppedChan <- 1
							}
						}
					} else {
						tcpSocket.Send(nodeList.List[i].IP, port, message.HeartbeatRequest{IP: ip})
					}
				}
			}
		}
	}
}

func onHeartbeatRequest(ip, port string, tcpSocket *tcp.TCPSocket) {
	var connected = true
	for connected {
		node, ok := <-heartbeatRequestChan

		if ok {
			tcpSocket.Send(node.IP, port, message.HeartbeatResponse{IP: ip})
		} else {
			connected = false
		}
	}
}

func onHeartbeatResponse(ip, port string, nodeList *nodelist.NodeList) {
	var connected = true
	for connected {
		node, ok := <-heartbeatResponseChan

		if ok {
			for i := 0; i < len(nodeList.List); i++ {
				if nodeList.List[i].IP == node.IP {
					nodeList.List[i].IsSuspected = false

					if !nodeList.List[i].IsAlive {
						nodeList.List[i].IsAlive = true
						fmt.Println("Failure Detector: Node came back alive. (", node.IP, ")")
					}
					break
				}
			}
		} else {
			connected = false
		}
	}
}
