package main

import (
	app "lab6/controller/application"
	handler "lab6/controller/handler"
	leader "lab6/controller/leader/message"
	paxos "lab6/controller/paxos/message"

	"lab6/model/accountlist"
	"lab6/model/network/conn"
	"lab6/model/network/tcp"
	"lab6/model/network/udp"
	"lab6/model/nodelist"

	"fmt"
	"net"
	"strings"
)

var (
	exit = make(chan bool, 1)
	list []nodelist.Node
)

type IPAddressNotFound struct {
	Message string
}

func (e *IPAddressNotFound) Error() string {
	return fmt.Sprintf("at %v, %s",
		e.Message)
}

func main() {
	port := "27015"
	ip, err := getLocalIPAddress()

	if err == nil {
		connectionList := conn.NewConnectionList()
		tcpSocket := tcp.NewTCPSocket(port, connectionList)

		go tcpSocket.StartSocket()
		go handler.HandleTCPMessages(tcpSocket)

		go udp.UDPSocket(port)
		go handler.HandleUDPMessages()

		client(ip, port, tcpSocket)

		<-exit
	} else {
		fmt.Println("System: Could not get local IP address. Exiting program")
	}
}

func client(ip, port string, tcpSocket *tcp.TCPSocket) {
	go onSlotResponse()
	fmt.Println("Client: Looking for a Paxos system on the network...")

	var nodeIP = getLongestRunningNode(ip, port, tcpSocket)

	for {
		fmt.Println("Client: Select a transaction:")
		fmt.Println("Client:   1. Deposit")
		fmt.Println("Client:   2. Withdraw")
		fmt.Println("Client:   3. Transfer")
		fmt.Println("Client:   4. Balance")
		fmt.Print("Client: ")

		var action int
		fmt.Scanf("%d", &action)

		switch action {
		case 1:
			fmt.Println("Client: Deposit chosen.")
			fmt.Println("Client: Enter account number: ")
			fmt.Print("Client: ")

			var account string
			fmt.Scanf("%s", &account)

			fmt.Println("Client: Enter amount: ")
			fmt.Print("Client: ")

			var amount int
			fmt.Scanf("%d", &amount)

			tcpSocket.Send(nodeIP, port, paxos.Received{IP: ip, Value: app.Deposit{AccountNumber: account, Amount: amount}})
		case 2:
			fmt.Println("Client: Withdraw chosen.")
			fmt.Println("Client: Enter account number: ")
			fmt.Print("Client: ")

			var account string
			fmt.Scanf("%s", &account)

			fmt.Println("Client: Enter amount: ")
			fmt.Print("Client: ")

			var amount int
			fmt.Scanf("%d", &amount)

			tcpSocket.Send(nodeIP, port, paxos.Received{IP: ip, Value: app.Withdraw{AccountNumber: account, Amount: amount}})
		case 3:
			fmt.Println("Client: Transfer chosen.")
			fmt.Println("Client: Enter account number to transfer from: ")
			fmt.Print("Client: ")

			var fromAccount string
			fmt.Scanf("%s", &fromAccount)

			fmt.Println("Client: Enter account number to transfer to: ")
			fmt.Print("Client: ")

			var toAccount string
			fmt.Scanf("%s", &toAccount)

			fmt.Println("Client: Enter amount: ")
			fmt.Print("Client: ")

			var amount int
			fmt.Scanf("%d", &amount)

			tcpSocket.Send(nodeIP, port, paxos.Received{IP: ip, Value: app.Transfer{FromAccount: fromAccount, ToAccount: toAccount, Amount: amount}})
		case 4:
			fmt.Println("Client: Balance chosen.")
			fmt.Println("Client: Enter account number: ")
			fmt.Print("Client: ")

			var account string
			fmt.Scanf("%s", &account)

			tcpSocket.Send(nodeIP, port, paxos.Received{IP: ip, Value: app.Balance{AccountNumber: account}})
		}
	}
}

func onSlotResponse() {
	var connected = true
	for connected {
		msg := <-paxos.SlotResponseChan

		switch action := msg.Message.(type) {
		case app.Deposit:
			fmt.Println("Deposited", action.Amount, "to account number:", action.AccountNumber)
		case app.Withdraw:
			fmt.Println("Withdrew", action.Amount, "from account number:", action.AccountNumber)
		case app.Transfer:
			fmt.Println("Transfered", action.Amount, "to account number:", action.ToAccount, "from account number:", action.FromAccount)
		case accountlist.Account:
			fmt.Println("The balance in account", action.AccountNumber, "is:", action.Balance)
		case string:
			fmt.Println("Received debug message:", action)
		}

		fmt.Print("Client: ")
	}
}

func onNodeListResponse() (list []nodelist.Node) {
	resp, ok := <-leader.NodeListResponseChan

	if ok {
		list = resp.NodeList
	}

	return
}

func onLeaderIPResponse(ip string) string {
	node, ok := <-leader.LeaderIPResponseChan

	if ok {
		return node.IP
	}

	return ip
}

func getLongestRunningNode(ip, port string, tcpSocket *tcp.TCPSocket) string {
	udp.Broadcast("255.255.255.255", port, leader.LEADERIP)
	leaderIP := onLeaderIPResponse(ip)

	if leaderIP != ip {
		fmt.Println("Client: The leader of the Paxos system is", leaderIP)

		fmt.Println("Client: Getting list of nodes in the Paxos system")
		tcpSocket.Send(leaderIP, port, leader.NodeListRequest{IP: ip})

		list = onNodeListResponse()
		fmt.Println("Client: Got node list. Selecting the longest running node to send messages to.")

		var nodeAddress = list[0].IP
		var starttime = list[0].Starttime
		for i := 1; i < len(list); i++ {
			if list[i].IsAlive {
				if list[i].Starttime < starttime {
					nodeAddress = list[i].IP
					starttime = list[i].Starttime
				}
			}
		}

		return nodeAddress
	}

	return ""
}

func getLocalIPAddress() (string, error) {
	var addrs, _ = net.InterfaceAddrs()
	for i := 0; i < len(addrs); i++ {
		if strings.Contains(addrs[i].String(), "/24") {
			return strings.Replace(addrs[i].String(), "/24", "", 1), nil
		}
	}

	return "", &IPAddressNotFound{"IP Address not found"}
}
