package main

import (
	handler "lab6/controller/handler"
	leader "lab6/controller/leader/election"
	paxos "lab6/controller/paxos/message"

	"lab6/controller/paxos/acceptor"
	"lab6/controller/paxos/learner"
	"lab6/controller/paxos/proposer"

	"lab6/model/network/conn"
	"lab6/model/network/tcp"
	"lab6/model/network/udp"
	"lab6/model/nodelist"
	"lab6/model/slotlist"

	"fmt"
	"net"
	"strings"
)

var (
	exit     = make(chan bool, 1)
	nodeList = &nodelist.Instance

	isLeader    = false
	slotList    = slotlist.NewSlotList()
	packageLoss = paxos.PackageLoss{Lost: false}
)

type IPAddressNotFound struct {
	Message string
}

func (e *IPAddressNotFound) Error() string {
	return fmt.Sprintf("at %v, %s",
		e.Message)
}

func main() {
	port := "27015"
	ip, err := getLocalIPAddress()

	if err == nil {
		connectionList := conn.NewConnectionList()
		tcpSocket := tcp.NewTCPSocket(port, connectionList)

		go tcpSocket.StartSocket()
		go handler.HandleTCPMessages(tcpSocket)

		go udp.UDPSocket(port)
		go handler.HandleUDPMessages()

		server(ip, port, tcpSocket, nodeList)

		<-exit
	} else {
		fmt.Println("System: Could not get local IP address. Exiting program")
	}
}

func server(ip, port string, tcpSocket *tcp.TCPSocket, nodeList *nodelist.NodeList) {
	//go onSimulate()
	go onReceived(port, tcpSocket)
	go learner.Learner(ip, port, tcpSocket, slotList)
	go leader.Election(ip, port, tcpSocket, nodeList)
	go proposer.Proposer(ip, port, tcpSocket, nodeList, slotList)
	acceptor.Acceptor(ip, port, tcpSocket, nodeList, slotList, &packageLoss)
}

func onSimulate() {
	var connected = true
	for connected {
		fmt.Println("Paxos: Simulate package loss? (Y/N)")
		var key string
		fmt.Scanf("%s", &key)

		if key == "Y" {
			packageLoss.Lost = true
			fmt.Println("Paxos: Package loss simulation enabled")
		} else if key == "N" {
			packageLoss.Lost = false
			fmt.Println("Paxos: Package loss simulation disabled")
		} else {
			fmt.Println("Paxos: The input was not valid")
		}
	}
}

func onReceived(port string, tcpSocket *tcp.TCPSocket) {
	var connected = true
	for connected {
		msg, ok := <-paxos.ReceivedChan

		if ok {
			if leader.CurrentLeader.IP != "" {
				tcpSocket.Send(leader.CurrentLeader.IP, port, paxos.Trust{IP: msg.IP, Value: msg.Value})
			}
		} else {
			connected = false
		}
	}
}

func getLocalIPAddress() (string, error) {
	var addrs, _ = net.InterfaceAddrs()
	for i := 0; i < len(addrs); i++ {
		if strings.Contains(addrs[i].String(), "/24") {
			return strings.Replace(addrs[i].String(), "/24", "", 1), nil
		}
	}

	return "", &IPAddressNotFound{"IP Address not found"}
}
