package main

import (
	app "lab6/controller/application"
	handler "lab6/controller/handler"
	leader "lab6/controller/leader/message"
	paxos "lab6/controller/paxos/message"

	"lab6/model/accountlist"
	"lab6/model/network/conn"
	"lab6/model/network/tcp"
	"lab6/model/network/udp"
	"lab6/model/nodelist"

	"fmt"
	"net"
	"strings"
)

var (
	exit = make(chan bool, 1)

	waitForAck bool
	list       []nodelist.Node
)

type IPAddressNotFound struct {
	Message string
}

func (e *IPAddressNotFound) Error() string {
	return fmt.Sprintf("at %v, %s",
		e.Message)
}

func main() {
	port := "27015"
	ip, err := getLocalIPAddress()

	if err == nil {
		connectionList := conn.NewConnectionList()
		tcpSocket := tcp.NewTCPSocket(port, connectionList)

		go tcpSocket.StartSocket()
		go handler.HandleTCPMessages(tcpSocket)

		go udp.UDPSocket(port)
		go handler.HandleUDPMessages()

		server(ip, port, tcpSocket)

		<-exit
	} else {
		fmt.Println("System: Could not get local IP address. Exiting program")
	}
}

func server(ip, port string, tcpSocket *tcp.TCPSocket) {
	fmt.Println("Server: Looking for a Paxos system on the network...")

	var accountList = accountlist.NewAccountList()
	var nodeIP = getLongestRunningNode(ip, port, tcpSocket)

	fmt.Println("Server: Connected. Running slot extract.")

	var connected = true
	for connected {
		tcpSocket.Send(nodeIP, port, paxos.SlotRequest{IP: ip})

		msg := <-paxos.SlotResponseChan

		switch action := msg.Message.(type) {
		case app.Deposit:
			accountList.Deposit(action.AccountNumber, action.Amount)
			fmt.Println("Depositing", action.Amount, "to account number:", action.AccountNumber)
			tcpSocket.Send(msg.IP, port, paxos.SlotResponse{IP: ip, Message: action})
		case app.Withdraw:
			accountList.Withdraw(action.AccountNumber, action.Amount)
			fmt.Println("Withdrawing", action.Amount, "from account number:", action.AccountNumber)
			tcpSocket.Send(msg.IP, port, paxos.SlotResponse{IP: ip, Message: action})
		case app.Transfer:
			accountList.Transfer(action.FromAccount, action.ToAccount, action.Amount)
			fmt.Println("Transfering", action.Amount, "to account number:", action.ToAccount, "from account number:", action.FromAccount)
			tcpSocket.Send(msg.IP, port, paxos.SlotResponse{IP: ip, Message: action})
		case app.Balance:
			var balance = accountList.Balance(action.AccountNumber)
			fmt.Println("The balance in account", action.AccountNumber, "is:", balance)
			tcpSocket.Send(msg.IP, port, paxos.SlotResponse{IP: ip, Message: accountlist.Account{AccountNumber: action.AccountNumber, Balance: balance}})
		case string:
			fmt.Println("Received debug message:", action)
			tcpSocket.Send(msg.IP, port, paxos.SlotResponse{IP: ip, Message: action})
		}
	}
}

func onNodeListResponse() (list []nodelist.Node) {
	resp, ok := <-leader.NodeListResponseChan

	if ok {
		list = resp.NodeList
	}

	return
}

func onLeaderIPResponse(ip string) string {
	node, ok := <-leader.LeaderIPResponseChan

	if ok {
		return node.IP
	}

	return ip
}

func getLongestRunningNode(ip, port string, tcpSocket *tcp.TCPSocket) string {
	udp.Broadcast("255.255.255.255", port, leader.LEADERIP)
	var leaderIP = onLeaderIPResponse(ip)

	if leaderIP != ip {
		fmt.Println("Client: The leader of the Paxos system is", leaderIP)

		fmt.Println("Client: Getting list of nodes in the Paxos system")
		tcpSocket.Send(leaderIP, port, leader.NodeListRequest{IP: ip})

		list = onNodeListResponse()
		fmt.Println("Client: Got node list. Selecting the longest running node to send messages to.")

		var nodeAddress = list[0].IP
		var starttime = list[0].Starttime
		for i := 1; i < len(list); i++ {
			if list[i].IsAlive {
				if list[i].Starttime < starttime {
					nodeAddress = list[i].IP
					starttime = list[i].Starttime
				}
			}
		}

		return nodeAddress
	}

	return ""
}

func getLocalIPAddress() (string, error) {
	var addrs, _ = net.InterfaceAddrs()
	for i := 0; i < len(addrs); i++ {
		if strings.Contains(addrs[i].String(), "/24") {
			return strings.Replace(addrs[i].String(), "/24", "", 1), nil
		}
	}

	return "", &IPAddressNotFound{"IP Address not found"}
}
