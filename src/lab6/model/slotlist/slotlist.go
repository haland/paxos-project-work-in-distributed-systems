package slotlist

import (
	"lab6/controller/paxos/message"
	"sync"
	"time"
)

type SlotList struct {
	alpha int
	size  int
	array []*message.Learn
	mutex sync.Mutex
}

func NewSlotList() *SlotList {
	return &SlotList{alpha: 1, size: 0, array: make([]*message.Learn, 10)}
}

func (s *SlotList) Add(i *message.Learn, j int) bool {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if s.size <= j {
		for k := s.size; k < j*2+1; k++ {
			s.array = append(s.array, nil)
		}
	}

	if s.array[j] == nil {
		s.size = j
		s.array[j] = i
		return true
	}

	return false
}

func (s *SlotList) Remove(j int) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if s.size > j {
		s.array[j] = nil
	}
}

func (s *SlotList) Get(j int) *message.Learn {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if j < s.size && s.array[j] != nil {
		return s.array[j]
	}

	return nil
}

func (s *SlotList) Pull() (string, interface{}) {
	min := s.alpha
	s.alpha = s.alpha + 1
	for s.array[min] == nil {
		time.Sleep(time.Second / 200)
	}

	return s.array[min].ClientIP, s.array[min].RoundValue
}

func (s *SlotList) Size() int {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	return s.size
}

func (s *SlotList) UnfilledSlots() (u []int) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	for i := 0; i < s.size; i++ {
		if s.array[i] == nil {
			u = append(u, i)
		}
	}

	return
}
