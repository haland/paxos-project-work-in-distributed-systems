package accountlist

import "encoding/gob"

func init() {
	gob.Register(Account{})
}

type Account struct {
	Balance       int
	AccountNumber string
}

type accountList struct {
	list []Account
}

func NewAccountList() *accountList {
	return &accountList{list: make([]Account, 0)}
}

func (a *accountList) addAccount(_accountNumber string, _balance int) {
	a.list = append(a.list, Account{Balance: _balance, AccountNumber: _accountNumber})
}

func (a *accountList) Deposit(_accountNumber string, _amount int) {
	if a.contains(_accountNumber) == -1 {
		a.addAccount(_accountNumber, 0)
	}

	for i := 0; i < len(a.list); i++ {
		if a.list[i].AccountNumber == _accountNumber {
			a.list[i].Balance = a.list[i].Balance + _amount
		}
	}
}

func (a *accountList) Withdraw(_accountNumber string, _amount int) {
	if a.contains(_accountNumber) == -1 {
		a.addAccount(_accountNumber, 0)
	}

	for i := 0; i < len(a.list); i++ {
		if a.list[i].AccountNumber == _accountNumber {
			a.list[i].Balance = a.list[i].Balance - _amount
		}
	}
}

func (a *accountList) Transfer(_fromAccount, _toAccount string, _amount int) {
	if a.contains(_fromAccount) == -1 {
		a.addAccount(_fromAccount, 0)
	}

	if a.contains(_toAccount) == -1 {
		a.addAccount(_toAccount, 0)
	}

	from := a.contains(_fromAccount)
	to := a.contains(_toAccount)

	a.list[from].Balance = a.list[from].Balance - _amount
	a.list[to].Balance = a.list[to].Balance + _amount
}

func (a *accountList) Balance(_accountNumber string) int {
	amount := -1
	idx := a.contains(_accountNumber)
	if idx != -1 {
		amount = a.list[idx].Balance
	}

	return amount
}

func (a *accountList) contains(_accountNumber string) int {
	idx := -1
	for i := 0; i < len(a.list); i++ {
		if a.list[i].AccountNumber == _accountNumber {
			idx = i
		}
	}

	return idx
}
