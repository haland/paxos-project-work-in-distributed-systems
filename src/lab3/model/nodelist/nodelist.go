package nodelist

import (
	"lab3/model/network/message"
)

var List NodeList

type NodeList struct {
	NodeList []message.Node
}

func init() {
	if List.NodeList == nil {
		List = NodeList{make([]message.Node, 0)}
	}
}

func (n *NodeList) Add(node message.Node) {
	var found = false

	for i := 0; i < len(n.NodeList); i++ {
		if n.NodeList[i].IP == node.IP {
			n.NodeList[i].ALIVE = node.ALIVE
			n.NodeList[i].IS_LEADER = node.IS_LEADER
			n.NodeList[i].SUSPECTED = node.SUSPECTED
			n.NodeList[i].START_TIME = node.START_TIME
			found = true
			break
		}
	}

	if !found {
		n.NodeList = append(n.NodeList, node)
	}
}

func (n *NodeList) Contains(node message.Node) bool {
	for i := 0; i < len(n.NodeList); i++ {
		if n.NodeList[i].IP == node.IP {
			return true
		}
	}

	return false
}
