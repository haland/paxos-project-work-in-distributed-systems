package message

import (
	"encoding/gob"
	"lab3/model/network/tcp"
	"lab3/model/network/udp"
	"strconv"
	"strings"
)

func init() {
	gob.Register(HeartbeatRequest{})
	gob.Register(HeartbeatResponse{})
	gob.Register(NewNodeStarted{})
	gob.Register(NodeListRequest{})
	gob.Register(NodeListResponse{})
} // Only TCP message types

var (
	LEADERREQUEST  = "LEADERREQUEST"
	LEADERRESPONSE = "LEADERRESPONSE"
)

type Node struct {
	IP         string
	ALIVE      bool
	SUSPECTED  bool
	IS_LEADER  bool
	START_TIME int64
}

type HeartbeatRequest struct {
	IP string
} // TCP

type HeartbeatResponse struct {
	IP string
} // TCP

type NodeListRequest struct {
	IP string
} // TCP

type NodeListResponse struct {
	IP       string
	NodeList []Node
} // TCP

type NewNodeStarted struct {
	IP string
} // TCP

type LeaderRequest struct {
	IP string
} // UDP

type LeaderResponse struct {
	IP         string
	START_TIME int64
} // UDP

func HandleTCPMessages(tcpChan chan tcp.TCPMessage, heartbeatRequestChan chan HeartbeatRequest, heartbeatResponseChan chan HeartbeatResponse, nodeListRequestChan chan NodeListRequest, nodeListResponseChan chan NodeListResponse, newNodeStartedChan chan NewNodeStarted) {
	var connected = true
	for connected {
		tcpMessage, ok := <-tcpChan

		if ok {
			switch tcpMessage.Message.(type) {
			case HeartbeatRequest:
				heartbeatRequestChan <- HeartbeatRequest{tcpMessage.IP}
			case HeartbeatResponse:
				heartbeatResponseChan <- HeartbeatResponse{tcpMessage.IP}
			case NodeListRequest:
				nodeListRequestChan <- NodeListRequest{tcpMessage.IP}
			case NodeListResponse:
				nodeListResponseChan <- NodeListResponse{tcpMessage.IP, tcpMessage.Message.(NodeListResponse).NodeList}
			case NewNodeStarted:
				newNodeStartedChan <- NewNodeStarted{tcpMessage.IP}
			}
		} else {
			connected = false
		}
	}

	close(heartbeatRequestChan)
	close(heartbeatResponseChan)
	close(nodeListRequestChan)
	close(nodeListResponseChan)
	close(newNodeStartedChan)
}

func HandleUDPMessages(udpChan chan udp.UDPMessage, leaderRequestChan chan LeaderRequest, leaderResponseChan chan LeaderResponse) {
	var connected = true
	for connected {
		udpMessage, ok := <-udpChan

		if ok {
			if strings.Contains(udpMessage.Message, LEADERREQUEST) {
				leaderRequestChan <- LeaderRequest{udpMessage.IP}
			} else if strings.Contains(udpMessage.Message, LEADERRESPONSE) {
				msg, _ := strconv.ParseInt(strings.Replace(udpMessage.Message, LEADERRESPONSE, "", 1), 10, 64)
				leaderResponseChan <- LeaderResponse{IP: udpMessage.IP, START_TIME: msg}
			}
		} else {
			connected = false
		}
	}

	close(leaderRequestChan)
	close(leaderResponseChan)
}
