package udp

import (
	"fmt"
	"net"
	"os"
	"strings"
	"time"
)

type UDPMessage struct {
	IP      string
	Message string
}

func UDPSocket(port string, udpChan chan UDPMessage, exit chan int) {
	udpAddr, err := net.ResolveUDPAddr("udp4", strings.Join([]string{":", port}, ""))

	checkErr(err)
	listener, err := net.ListenUDP("udp4", udpAddr)

	checkErr(err)
	var connected = true

	for connected {
		var TIMEOUT = time.Now().Add(time.Second / 8)

		data := make([]byte, 4096)
		listener.SetDeadline(TIMEOUT)
		n, addr, err := listener.ReadFromUDP(data[0:])

		if err == nil {
			connected = send(udpChan, UDPMessage{addr.IP.String(), string(data[0:n])})
		} else {
			connected = send(udpChan, UDPMessage{})
		}
	}

	listener.Close()
	exit <- 1
}

func Broadcast(ip, port, message string) {
	udpAddr, err := net.ResolveUDPAddr("udp", strings.Join([]string{ip, ":", port}, ""))

	checkErr(err)
	conn, err := net.DialUDP("udp", nil, udpAddr)

	checkErr(err)
	conn.Write([]byte(message))
}

func send(c chan UDPMessage, t UDPMessage) bool {
	defer func() { recover() }()
	c <- t
	return true
}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
