package tcp

import (
	"encoding/gob"
	"fmt"
	"io"
	"net"
	"os"
	"strings"
	"time"
)

func init() {
	gob.Register(TCPMessage{})
}

var LocalIP string

type TCPMessage struct {
	IP      string
	Message interface{}
}

func TCPSocket(port string, tcpChan chan TCPMessage, exit chan int) {
	tcpAddr, err := net.ResolveTCPAddr("tcp", strings.Join([]string{":", port}, ""))

	checkErr(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)

	checkErr(err)

	var connected = true
	for connected {
		var TIMEOUT = time.Now().Add(time.Second)

		listener.SetDeadline(TIMEOUT)
		conn, err := listener.Accept()

		if err == nil {
			decoder := gob.NewDecoder(conn)

			var msg interface{}
			err = decoder.Decode(&msg)
			if err != nil {
				if err != io.EOF {
					checkErr(err)
				}
			} else {
				addr := conn.RemoteAddr().String()
				connected = send(tcpChan, TCPMessage{addr[0:strings.Index(addr, ":")], msg})
			}

			conn.Close()
		} else {
			connected = send(tcpChan, TCPMessage{})
		}
	}

	listener.Close()
	exit <- 1
}

func Send(ip string, port string, msg interface{}) (err error) {
	conn, err := net.Dial("tcp", strings.Join([]string{ip, ":", port}, ""))

	if err == nil {
		encoder := gob.NewEncoder(conn)
		encoder.Encode(&msg)
	}

	return
}

func send(c chan TCPMessage, t TCPMessage) bool {
	defer func() { recover() }()
	c <- t
	return true
}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
