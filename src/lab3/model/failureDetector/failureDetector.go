package failureDetector

import (
	"fmt"
	"lab3/model/network/message"
	"lab3/model/network/tcp"
	"lab3/model/nodelist"
	"time"
)

var (
	SUSPECTED = false
	DELTA     = time.Second / 16

	port    string
	localIP string
)

func FailureDetector(_localIP string, _port string, list *nodelist.NodeList, tcpChan chan tcp.TCPMessage, heartbeatRequestChan chan message.HeartbeatRequest, heartbeatResponseChan chan message.HeartbeatResponse, nodeListRequestChan chan message.NodeListRequest, nodeListResponseChan chan message.NodeListResponse) {
	port = _port
	localIP = _localIP

	SUSPECTED = false

	var TIMEOUT = time.Second / 4

	go onHeartbeatRequest(heartbeatRequestChan)
	go onHeartbeatResponse(list, heartbeatResponseChan)

	go onNodeListRequest(list, nodeListRequestChan)
	go onNodeListResponse(list, nodeListResponseChan)

	var connected = true
	for connected {
		if SUSPECTED {
			TIMEOUT = TIMEOUT + DELTA
		}

		SUSPECTED = false
		time.Sleep(TIMEOUT)

		for i := 0; i < len(list.NodeList); i++ {
			if list.NodeList[i].IP != localIP {
				if !list.NodeList[i].ALIVE && !list.NodeList[i].SUSPECTED {
					list.NodeList[i].SUSPECTED = true
				} else if list.NodeList[i].ALIVE && list.NodeList[i].SUSPECTED {
					SUSPECTED = true
					list.NodeList[i].SUSPECTED = false
				}

				if list.NodeList[i].ALIVE && !list.NodeList[i].SUSPECTED {
					list.NodeList[i].SUSPECTED = true
					err := tcp.Send(list.NodeList[i].IP, port, message.HeartbeatRequest{})

					if err != nil {
						list.NodeList[i].ALIVE = false
						fmt.Println(list.NodeList[i].IP, "timed out.")

						if list.NodeList[i].IS_LEADER {
							connected = false
						}
					}
				} else {
					tcp.Send(list.NodeList[i].IP, port, message.HeartbeatRequest{})
				}
			}
		}
	}

	close(tcpChan)
}

func onHeartbeatRequest(heartbeatRequestChan chan message.HeartbeatRequest) {
	var connected = true
	for connected {
		node, ok := <-heartbeatRequestChan

		if ok {
			tcp.Send(node.IP, port, message.HeartbeatResponse{})
		} else {
			connected = false
		}
	}
}

func onHeartbeatResponse(list *nodelist.NodeList, heartbeatResponseChan chan message.HeartbeatResponse) {
	var connected = true
	for connected {
		node, ok := <-heartbeatResponseChan

		if ok {
			for i := 0; i < len(list.NodeList); i++ {
				if list.NodeList[i].IP == node.IP {
					list.NodeList[i].SUSPECTED = false

					if !list.NodeList[i].ALIVE {
						list.NodeList[i].ALIVE = true
						fmt.Println(node.IP, "came back alive")
					}

					break
				}
			}
		} else {
			connected = false
		}
	}
}

func onNodeListRequest(list *nodelist.NodeList, nodeListRequestChan chan message.NodeListRequest) {
	var connected = true
	for connected {
		node, ok := <-nodeListRequestChan

		if ok {
			tcp.Send(node.IP, port, message.NodeListResponse{IP: localIP, NodeList: list.NodeList})
		} else {
			connected = false
		}
	}
}

func onNodeListResponse(list *nodelist.NodeList, nodeListResponseChan chan message.NodeListResponse) {
	var connected = true
	for connected {
		response, ok := <-nodeListResponseChan

		if ok {
			fmt.Println("Received new node list from", response.IP)
			for i := 0; i < len(response.NodeList); i++ {
				list.Add(response.NodeList[i])
			}
		} else {
			connected = false
		}
	}
}
