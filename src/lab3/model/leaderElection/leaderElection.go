package leaderElection

import (
	"fmt"
	"lab3/model/network/message"
	"lab3/model/network/udp"
	"lab3/model/nodelist"
	"time"
)

var (
	TIMEOUT = time.Second
	list    = nodelist.List
)

type NoLeaderFound struct {
	Message string
}

func (nlf *NoLeaderFound) Error() string {
	return fmt.Sprintf("at %v, %s",
		nlf.Message)
}

func LeaderElection(port string, list *nodelist.NodeList) (leader message.Node, leaderResponse message.LeaderResponse, err error) {
	if len(list.NodeList) > 0 {
		leader = findNewLeader(list.NodeList)
		return leader, message.LeaderResponse{}, nil
	} else {
		var (
			exit               = make(chan int)
			udpChan            = make(chan udp.UDPMessage)
			leaderRequestChan  = make(chan message.LeaderRequest, 5)
			leaderResponseChan = make(chan message.LeaderResponse, 5)
		)

		udp.Broadcast("255.255.255.255", port, message.LEADERREQUEST)

		go udp.UDPSocket(port, udpChan, exit)
		go message.HandleUDPMessages(udpChan, leaderRequestChan, leaderResponseChan)

		leaderResponse = onLeaderResponse(udpChan, leaderResponseChan)

		close(udpChan)
		<-exit

		if len(leaderResponse.IP) > 0 {
			return message.Node{}, leaderResponse, nil
		}

		return message.Node{}, leaderResponse, &NoLeaderFound{"No leader found"}
	}

	return
}

func onLeaderResponse(udpChan chan udp.UDPMessage, leaderResponseChan chan message.LeaderResponse) (leaderResponse message.LeaderResponse) {
	var connected = true
	for connected {
		var timeout = make(chan bool, 1)

		go func() {
			time.Sleep(TIMEOUT)
			timeout <- true
		}()

		select {
		case leaderMessage, ok := <-leaderResponseChan:
			if ok {
				leaderResponse = leaderMessage
				connected = false
			} else {
				connected = false
			}

			<-timeout
		case <-timeout:
			connected = false
		}

		close(timeout)
	}

	return
}

func findNewLeader(nodeList []message.Node) (node message.Node) {
	var potentialLeaders = make([]message.Node, 0)
	for i := 0; i < len(nodeList); i++ {
		if !nodeList[i].IS_LEADER && nodeList[i].ALIVE {
			potentialLeaders = append(potentialLeaders, nodeList[i])
		}
	}

	node = potentialLeaders[0]
	for i := 0; i < len(potentialLeaders); i++ {
		if potentialLeaders[i].START_TIME < node.START_TIME {
			node = potentialLeaders[i]
		}
	}

	return
}
