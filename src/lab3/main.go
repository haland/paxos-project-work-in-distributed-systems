package main

import (
	"fmt"
	"lab3/controller/machine"
	"lab3/model/nodelist"
)

var list = nodelist.List

func main() {
	port := readPort()

	for {
		machine.Machine(port, &list)
	}
}

func readPort() (port string) {
	fmt.Println("Please enter port for the distributed system to work on")
	fmt.Scanf("%s", &port)
	return
}
