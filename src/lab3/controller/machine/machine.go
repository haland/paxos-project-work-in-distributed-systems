package machine

import (
	"fmt"
	"lab3/controller/leader"
	"lab3/controller/node"
	"lab3/model/leaderElection"
	"lab3/model/nodelist"
	"net"
	"strings"
)

var START_TIME int64

type IPAddressNotFound struct {
	Message string
}

func (e *IPAddressNotFound) Error() string {
	return fmt.Sprintf("at %v, %s",
		e.Message)
}

func Machine(port string, list *nodelist.NodeList) {
	localIP, err := getLocalIPAddress()

	if err == nil {
		leaderNode, leaderResponse, err := leaderElection.LeaderElection(port, list)
		clearLeaderReferences(list)

		if len(leaderNode.IP) > 0 {
			if leaderNode.START_TIME == START_TIME {
				fmt.Println("Leader is down. This is now the new leader.")
				leader.Leader(localIP, port, list)
			} else {
				fmt.Println("Leader is down. The new leader is", leaderNode.IP)
				node.Node(localIP, leaderNode.IP, port, list)
			}
		} else if len(leaderResponse.IP) > 0 && err == nil {
			fmt.Println("Found leader at", leaderResponse.IP)
			fmt.Println("Starting up as node.")
			START_TIME = leaderResponse.START_TIME
			node.Node(localIP, leaderResponse.IP, port, list)
		} else if err != nil {
			fmt.Println("No leader found. Setting self as leader")
			leader.Leader(localIP, port, list)
		}
	} else {
		fmt.Println("Can't find IP address. Exiting program")
	}
}

func clearLeaderReferences(list *nodelist.NodeList) {
	for i := 0; i < len(list.NodeList); i++ {
		if list.NodeList[i].IS_LEADER {
			list.NodeList[i].IS_LEADER = false
		}
	}
}

func getLocalIPAddress() (string, error) {
	var addrs, _ = net.InterfaceAddrs()
	for i := 0; i < len(addrs); i++ {
		if strings.Contains(addrs[i].String(), "/24") {
			return strings.Replace(addrs[i].String(), "/24", "", 1), nil
		}
	}

	return "", &IPAddressNotFound{"IP Address not found"}
}
