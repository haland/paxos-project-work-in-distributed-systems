package leader

import (
	"fmt"
	"lab3/model/failureDetector"
	"lab3/model/network/message"
	"lab3/model/network/tcp"
	"lab3/model/network/udp"
	"lab3/model/nodelist"
	"strconv"
	"strings"
	"time"
)

var (
	DELAY = time.Second / 8

	port    string
	localIP string
)

func Leader(_localIP, _port string, list *nodelist.NodeList) {
	port = _port
	localIP = _localIP

	var (
		exit    = make(chan int)
		tcpChan = make(chan tcp.TCPMessage)
		udpChan = make(chan udp.UDPMessage)

		leaderRequestChan  = make(chan message.LeaderRequest)
		leaderResponseChan = make(chan message.LeaderResponse)

		heartbeatRequestChan  = make(chan message.HeartbeatRequest)
		heartbeatResponseChan = make(chan message.HeartbeatResponse)
		nodeListRequestChan   = make(chan message.NodeListRequest)
		nodeListResponseChan  = make(chan message.NodeListResponse)
		newNodeStartedChan    = make(chan message.NewNodeStarted)
	)

	go onLeaderRequest(list, leaderRequestChan, newNodeStartedChan)
	go message.HandleUDPMessages(udpChan, leaderRequestChan, leaderResponseChan)
	go udp.UDPSocket(port, udpChan, exit)

	go failureDetector.FailureDetector(localIP, port, list, tcpChan, heartbeatRequestChan, heartbeatResponseChan, nodeListRequestChan, nodeListResponseChan)
	go message.HandleTCPMessages(tcpChan, heartbeatRequestChan, heartbeatResponseChan, nodeListRequestChan, nodeListResponseChan, newNodeStartedChan)
	go tcp.TCPSocket(port, tcpChan, exit)

	var startTime = time.Now().Unix()
	list.Add(message.Node{IP: localIP, ALIVE: true, SUSPECTED: false, IS_LEADER: true, START_TIME: startTime})

	for i := 0; i < len(list.NodeList); i++ {
		if !list.NodeList[i].IS_LEADER {
			tcp.Send(list.NodeList[i].IP, port, message.NodeListResponse{IP: localIP, NodeList: list.NodeList})
			fmt.Println("Sending list to", list.NodeList[i].IP)
		}
	}

	<-exit
}

func onLeaderRequest(list *nodelist.NodeList, leaderRequestChan chan message.LeaderRequest, newNodeStartedChan chan message.NewNodeStarted) {
	var connected = true
	for connected {
		node, ok := <-leaderRequestChan

		if ok {
			var startTime = time.Now().Unix()
			var startTimeString = strconv.FormatInt(startTime, 10)
			udp.Broadcast(node.IP, port, strings.Join([]string{message.LEADERRESPONSE, startTimeString}, ""))

			fmt.Println(node.IP, "added to list. Waiting for the node to come online.")
			_, ok := <-newNodeStartedChan

			if ok {
				fmt.Println("Node online. Sending list to all nodes.")

				if list.Contains(message.Node{IP: node.IP}) {
					list.Add(message.Node{IP: node.IP, IS_LEADER: false, START_TIME: startTime})
				} else {
					list.Add(message.Node{IP: node.IP, ALIVE: true, SUSPECTED: false, IS_LEADER: false, START_TIME: startTime})
				}
			} else {
				connected = false
			}

			for i := 0; i < len(list.NodeList); i++ {
				if !list.NodeList[i].IS_LEADER {
					tcp.Send(list.NodeList[i].IP, port, message.NodeListResponse{IP: localIP, NodeList: list.NodeList})
					fmt.Println("Sending list to", list.NodeList[i].IP)
				}
			}

			fmt.Println("Current list contains:", list.NodeList)
		} else {
			connected = false
		}
	}
}
