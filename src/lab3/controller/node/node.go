package node

import (
	"lab3/model/failureDetector"
	"lab3/model/network/message"
	"lab3/model/network/tcp"
	"lab3/model/nodelist"
)

func Node(localIP, ip, port string, list *nodelist.NodeList) {
	var (
		exit    = make(chan int)
		tcpChan = make(chan tcp.TCPMessage)

		heartbeatRequestChan  = make(chan message.HeartbeatRequest)
		heartbeatResponseChan = make(chan message.HeartbeatResponse)
		nodeListRequestChan   = make(chan message.NodeListRequest)
		nodeListResponseChan  = make(chan message.NodeListResponse)
		newNodeStartedChan    = make(chan message.NewNodeStarted)
	)

	go failureDetector.FailureDetector(localIP, port, list, tcpChan, heartbeatRequestChan, heartbeatResponseChan, nodeListRequestChan, nodeListResponseChan)
	go message.HandleTCPMessages(tcpChan, heartbeatRequestChan, heartbeatResponseChan, nodeListRequestChan, nodeListResponseChan, newNodeStartedChan)
	go tcp.TCPSocket(port, tcpChan, exit)

	tcp.Send(ip, port, message.NewNodeStarted{})

	<-exit
}
