package leaderElection

import (
	failure "lab5/controller/failure/detector"

	"lab5/controller/leader/leader"
	"lab5/controller/leader/message"
	"lab5/controller/leader/node"
	"lab5/model/network/tcp"
	"lab5/model/network/udp"
	"lab5/model/nodelist"

	"fmt"
	"time"
)

var (
	CurrentLeader     = nodelist.Node{}
	leaderDroppedChan = make(chan int, 0)
)

type NoLeaderFound struct {
	Message string
}

func (nlf *NoLeaderFound) Error() string {
	return fmt.Sprintf("at %v, %s",
		nlf.Message)
}

func Election(ip, port string, tcpSocket *tcp.TCPSocket, nodeList *nodelist.NodeList) {
	go failure.Detector(ip, port, tcpSocket, nodeList, leaderDroppedChan)

	var starttime int64
	n, err := electLeader(port, nodeList)

	if err == nil {
		CurrentLeader.IP = n.IP
		fmt.Println("Leader Election: Found existing leader. (", CurrentLeader.IP, ")\nStarting up as node.")
		starttime = CurrentLeader.Starttime
		node.Node(ip, port, tcpSocket, CurrentLeader, nodeList, leaderDroppedChan)
	} else {
		fmt.Println("Leader Election: No leader found. Setting self as leader.")
		CurrentLeader.IP = ip
		leader.Leader(ip, port, starttime, tcpSocket, nodeList, leaderDroppedChan)
	}

	var connected = true
	for connected {
		fmt.Println("Leader Election: The leader is not reachable. Electing new leader now.")

		n, err := electLeader(port, nodeList)
		clearLeaderReferences(nodeList)

		if err == nil {
			CurrentLeader.IP = n.IP
			if CurrentLeader.IP == ip {
				fmt.Println("Leader Election: This is the new leader.")
				leader.Leader(ip, port, starttime, tcpSocket, nodeList, leaderDroppedChan)
			} else {
				fmt.Println("Leader Election: The new leader is", CurrentLeader.IP)
				nodeList.Get(CurrentLeader.IP).IsLeader = true
				node.Node(ip, port, tcpSocket, nodelist.Node{IP: CurrentLeader.IP}, nodeList, leaderDroppedChan)
			}
		}
	}
}

func electLeader(port string, nodeList *nodelist.NodeList) (leader nodelist.Node, err error) {
	if !nodeList.IsEmpty() {
		leader, err = findLeaderByList(nodeList)
	} else {
		udp.Broadcast("255.255.255.255", port, message.LEADERREQUEST)
		leader, err = onLeaderResponse()
	}

	return
}

//Listens on UDP for a response from a potential leader and times out after 
//a given dela if no leader is found in that time.

func onLeaderResponse() (leader nodelist.Node, err error) {
	var connected = true
	var leaderResponse message.LeaderResponse

	for connected {
		var timeout = make(chan bool, 1)

		go func() {
			time.Sleep(time.Second / 2)
			timeout <- true
		}()

		select {
		case leaderMessage, ok := <-message.LeaderResponseChan:
			if ok {
				leaderResponse = leaderMessage
				connected = false
			} else {
				err = &NoLeaderFound{"No leader found"}
				connected = false
			}
		case <-timeout:
			if connected {
				err = &NoLeaderFound{"No leader found"}
			}

			connected = false
		}
	}

	return nodelist.Node{IP: leaderResponse.IP, Starttime: leaderResponse.STARTTIME}, err
}

//Finds a leader from the list containing all nodes. The node with the longest 
//running time is chosen. Nodes that allready are leader or are not alive
//should not become the new leader.

func findLeaderByList(nodeList *nodelist.NodeList) (node nodelist.Node, err error) {
	var potentialLeaders = make([]nodelist.Node, 0)
	for i := 0; i < len(nodeList.List); i++ {
		if !nodeList.List[i].IsLeader && nodeList.List[i].IsAlive {
			potentialLeaders = append(potentialLeaders, nodeList.List[i])
		}
	}

	if len(potentialLeaders) <= 0 {
		return nodelist.Node{}, &NoLeaderFound{"No leader found"}
	}

	node = potentialLeaders[0]
	for i := 0; i < len(potentialLeaders); i++ {
		if potentialLeaders[i].Starttime < node.Starttime {
			node = potentialLeaders[i]
		}
	}

	return node, nil
}

//Clears the boolean variable for leader for every node in the node list. No
//node is supposed to be a leader when the leader election takes place.

func clearLeaderReferences(nodeList *nodelist.NodeList) {
	for i := 0; i < len(nodeList.List); i++ {
		if nodeList.List[i].IsLeader {
			nodeList.List[i].IsLeader = false
		}
	}
}
