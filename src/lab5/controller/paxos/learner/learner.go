package learner

import (
	"fmt"
	"strconv"

	"lab5/controller/paxos/message"
	"lab5/model/network/tcp"
	"lab5/model/slotlist"
)

var (
	lastRoundNumber int
	connected       = true
)

func Learner(ip, port string, tcpSocket *tcp.TCPSocket, slotList *slotlist.SlotList) {
	for connected {
		msg, ok := <-message.LearnChan

		if ok {
			if msg.RoundNumber >= lastRoundNumber {
				lastRoundNumber = msg.RoundNumber

				added := slotList.Add(msg, msg.SequenceNumber) //True if slot not taken, false if it is
				if added {
					fmt.Println("Paxos: Wrote value \"" + msg.RoundValue + "\" on round " + strconv.FormatInt((int64)(msg.RoundNumber), 10) + " and slot " + strconv.FormatInt((int64)(msg.SequenceNumber), 10))
				}
			}
		} else {
			connected = false
		}
	}
}
