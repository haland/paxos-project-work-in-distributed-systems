package acceptor

import (
	leader "lab5/controller/leader/election"
	paxos "lab5/controller/paxos/message"

	"lab5/model/network/tcp"
	"lab5/model/nodelist"
	"lab5/model/slotlist"

	"fmt"
)

var (
	round              = 0
	currentRoundNumber = 0

	lastRoundNumber    = 0
	lastSequenceNumber = 0
	lastRoundValue     = ""

	currentLeader = &leader.CurrentLeader
)

func Acceptor(ip, port string, tcpSocket *tcp.TCPSocket, nodeList *nodelist.NodeList, slotList *slotlist.SlotList, packageLoss *paxos.PackageLoss) {
	var connected = true
	for connected {
		select {
		case msg, ok := <-paxos.PrepareChan:

			if ok {
				if !packageLoss.Lost && msg.IP == currentLeader.IP {
					if msg.RoundNumber > round {
						round = msg.RoundNumber
						fmt.Println("Paxos: Promising round for leader")
						tcpSocket.Send(msg.IP, port, paxos.Promise{IP: ip, RoundNumber: round, LastRoundNumber: lastRoundNumber, LastRoundValue: lastRoundValue, LastSequenceNumber: lastSequenceNumber})
					}
				}
			} else {
				connected = false
			}
		case msg, ok := <-paxos.AcceptChan:
			if ok {
				if msg.IP == currentLeader.IP {
					if msg.RoundNumber >= currentRoundNumber {
						existing := slotList.Get(msg.SequenceNumber)

						if existing == nil {
							lastRoundNumber = msg.RoundNumber
							lastSequenceNumber = msg.SequenceNumber
							lastRoundValue = msg.RoundValue

							for i := 0; i < len(nodeList.List); i++ {
								tcpSocket.Send(nodeList.List[i].IP, port, paxos.Learn{SequenceNumber: msg.SequenceNumber, RoundNumber: msg.RoundNumber, RoundValue: msg.RoundValue})
							}
						} else {
							switch value := existing.(type) {
							case paxos.Learn:
								for i := 0; i < len(nodeList.List); i++ {
									tcpSocket.Send(nodeList.List[i].IP, port, value)
								}
							}
						}
					}
				}
			} else {
				connected = false
			}
		}
	}
}
