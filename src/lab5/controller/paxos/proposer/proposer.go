package proposer

import (
	leader "lab5/controller/leader/message"
	paxos "lab5/controller/paxos/message"

	"lab5/model/network/tcp"
	"lab5/model/nodelist"
	"lab5/model/slotlist"

	"fmt"
	"strconv"
	"time"
)

var (
	connected             = false
	currentRoundNumber    = 0
	currentSequenceNumber = 0

	quarum   = make(chan int, 0)
	promises = make([]paxos.Promise, 0)
)

func Proposer(ip, port string, tcpSocket *tcp.TCPSocket, nodeNumber int, nodeList *nodelist.NodeList, slotList *slotlist.SlotList) {
	<-leader.SetAsLeader
	currentRoundNumber = currentRoundNumber + 1
	fmt.Println("Paxos: Starting as proposer for the Paxos system")

	go onPromise(ip, port, nodeList)

	for len(nodeList.List) < nodeNumber {
		time.Sleep(time.Second / 10)
	}

	var isQuarum = false
	for !isQuarum {
		fmt.Println("Paxos: Sending prepare for a new round.")
		for i := 0; i < len(nodeList.List); i++ {
			tcpSocket.Send(nodeList.List[i].IP, port, paxos.Prepare{IP: ip, RoundNumber: currentRoundNumber})
		}

		timeout := make(chan int, 0)
		delay, _ := time.ParseDuration(strconv.FormatInt((int64)(len(nodeList.List)*1000/8), 10) + "ms")

		go func() {
			time.Sleep(delay)
			timeout <- 1
		}()

		select {
		case <-quarum:
			isQuarum = true
		case <-timeout:
			currentRoundNumber = currentRoundNumber + 1
			fmt.Println("Paxos: Not enough nodes to meet quarum. Increasing round number and trying again.")
		}
	}

	fmt.Println("Paxos: Quarum reached. Starting up round.")

	var any = true
	for i := 0; i < len(promises); i++ {
		if promises[i].LastRoundNumber != 0 {
			any = false
			break
		}
	}

	if !any {
		var idx = 0
		for i := 1; i < len(promises); i++ {
			if promises[i].LastRoundNumber > promises[idx].LastRoundNumber {
				idx = i
			}
		}

		for i := 0; i < len(nodeList.List); i++ {
			tcpSocket.Send(nodeList.List[i].IP, port, paxos.Accept{IP: ip, SequenceNumber: currentSequenceNumber, RoundNumber: currentRoundNumber, RoundValue: promises[idx].LastRoundValue})
		}
	}

	unfilled := slotList.UnfilledSlots()
	for i := 0; i < len(unfilled); i++ {
		for j := 0; j < len(nodeList.List); j++ {
			tcpSocket.Send(nodeList.List[j].IP, port, paxos.Accept{IP: ip, SequenceNumber: unfilled[i], RoundNumber: currentRoundNumber})
		}
	}

	var connected = true
	for connected {
		msg, ok := <-paxos.TrustChan

		if ok {
			currentSequenceNumber = currentSequenceNumber + 1
			go sendAccept(ip, port, tcpSocket, currentSequenceNumber, msg, nodeList)
		} else {
			connected = false
		}

	}
}

func sendAccept(ip, port string, tcpSocket *tcp.TCPSocket, sequenceNumber int, msg paxos.Trust, nodeList *nodelist.NodeList) {
	for i := 0; i < len(nodeList.List); i++ {
		tcpSocket.Send(nodeList.List[i].IP, port, paxos.Accept{IP: ip, SequenceNumber: sequenceNumber, RoundNumber: currentRoundNumber, RoundValue: msg.Value})
	}

	tcpSocket.Send(msg.IP, port, paxos.Accept{IP: ip, SequenceNumber: sequenceNumber, RoundNumber: currentRoundNumber, RoundValue: msg.Value})
}

func onPromise(ip, port string, nodeList *nodelist.NodeList) {
	var connected = true
	for connected {
		msg, ok := <-paxos.PromiseChan

		if ok {
			if msg.RoundNumber == currentRoundNumber {
				promises = append(promises, msg)
				fmt.Println("Paxos: Received promise from", msg.IP)

				if msg.LastSequenceNumber > currentSequenceNumber {
					currentSequenceNumber = msg.LastSequenceNumber
				}

				if len(promises) >= len(nodeList.List)/2+1 {
					quarum <- 1
				}
			}
		} else {
			connected = false
		}
	}
}
