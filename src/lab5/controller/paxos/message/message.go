package message

import "encoding/gob"

var (
	LearnChan    = make(chan Learn, 10)
	TrustChan    = make(chan Trust, 10)
	AcceptChan   = make(chan Accept, 10)
	PrepareChan  = make(chan Prepare, 10)
	PromiseChan  = make(chan Promise, 10)
	ReceivedChan = make(chan Received, 10)
)

func init() {
	gob.Register(Received{})
	gob.Register(Prepare{})
	gob.Register(Promise{})
	gob.Register(Accept{})
	gob.Register(Trust{})
	gob.Register(Learn{})
}

type PackageLoss struct {
	Lost bool
}

type Received struct {
	IP    string
	Value string
}

type Learn struct {
	SequenceNumber int
	RoundNumber    int
	RoundValue     string
}

type Trust struct {
	IP    string
	Value string
}

type Prepare struct {
	IP          string
	RoundNumber int
}

type Promise struct {
	IP                 string
	RoundNumber        int
	LastRoundNumber    int
	LastRoundValue     string
	LastSequenceNumber int
}

type Accept struct {
	IP             string
	SequenceNumber int
	RoundNumber    int
	RoundValue     string
}
