package main

import (
	handler "lab5/controller/handler"
	"lab5/controller/leader/message"
	paxos "lab5/controller/paxos/message"

	"lab5/model/network/conn"
	"lab5/model/network/tcp"
	"lab5/model/network/udp"
	"lab5/model/nodelist"
	"lab5/model/slotlist"

	"fmt"
	"net"
	"strconv"
	"strings"
	"time"
)

var (
	exit     = make(chan bool, 1)
	nodeList = &nodelist.Instance

	leader     string
	waitForAck bool
	list       []nodelist.Node

	wait = make(chan int)
	eof  = make(chan int)

	acceptChan           = paxos.AcceptChan
	leaderIPResponseChan = message.LeaderIPResponseChan
	nodeListResponseChan = message.NodeListResponseChan
)

type IPAddressNotFound struct {
	Message string
}

func (e *IPAddressNotFound) Error() string {
	return fmt.Sprintf("at %v, %s",
		e.Message)
}

func main() {
	port := "27015"
	ip, err := getLocalIPAddress()

	if err == nil {
		connectionList := conn.NewConnectionList()
		tcpSocket := tcp.NewTCPSocket(port, connectionList)

		go tcpSocket.StartSocket()
		go handler.HandleTCPMessages(tcpSocket)

		go udp.UDPSocket(port)
		go handler.HandleUDPMessages()

		client(ip, port, tcpSocket)

		<-exit
	} else {
		fmt.Println("System: Could not get local IP address. Exiting program")
	}
}

func client(ip, port string, tcpSocket *tcp.TCPSocket) {
	go onAccept()
	fmt.Println("Client: Looking for a Paxos system on the network...")

	var nodeIP = getLongestRunningNode(ip, port, tcpSocket)

	fmt.Println("Client: This client will now send 500 messages to the Paxos system. Wait for confirmation of Paxos learn before writing next value? (Y/N)")
	fmt.Print("Client: ")

	var do string
	fmt.Scanf("%s", &do)

	if do == "Y" {
		waitForAck = true
		fmt.Println("Client: Waiting for confirmation for each message.")
	} else if do == "N" {
		waitForAck = false
		fmt.Println("Paxos: Sending all messages at once.")
	} else {
		fmt.Println("Paxos: The input was not valid. Exiting...")
		return
	}

	start := time.Now().Nanosecond()

	for i := 0; i < 500; i++ {
		tcpSocket.Send(nodeIP, port, paxos.Received{IP: ip, Value: strconv.FormatInt((int64)(i+1), 10)})

		if waitForAck {
			var timeout = make(chan int, 0)
			go func() {
				time.Sleep(time.Second / 2)
				timeout <- 1
			}()

			select {
			case <-wait:
			//Do next round
			case <-timeout:
				fmt.Println("Client: Message timed out. Selecting new node and continuing...")
				nodeIP = getLongestRunningNode(ip, port, tcpSocket)
				i = i - 1
			}
		}
	}

	<-eof

	toe := time.Now().Nanosecond() - start

	if toe < 0 {
		fmt.Println(-toe, "nanoseconds")
	} else {
		fmt.Println(toe, "nanoseconds")
	}
}

func getLongestRunningNode(ip, port string, tcpSocket *tcp.TCPSocket) string {
	udp.Broadcast("255.255.255.255", port, message.LEADERIP)
	leader = onLeaderIPResponse(ip)

	if leader != ip {
		fmt.Println("Client: The leader of the Paxos system is", leader)

		fmt.Println("Client: Getting list of nodes in the Paxos system")
		tcpSocket.Send(leader, port, message.NodeListRequest{IP: ip})

		list = onNodeListResponse()
		fmt.Println("Client: Got node list. Selecting the longest running node to send messages to.")

		var nodeAddress = list[0].IP
		var starttime = list[0].Starttime
		for i := 1; i < len(list); i++ {
			if list[i].IsAlive {
				if list[i].Starttime < starttime {
					nodeAddress = list[i].IP
					starttime = list[i].Starttime
				}
			}
		}

		return nodeAddress
	}

	return ""
}

func onAccept() {
	var count = 0
	var slotList = slotlist.NewSlotList()

	var connected = true
	for connected {
		msg, ok := <-acceptChan

		if ok {
			if msg.RoundValue != "" {
				added := slotList.Add(msg, msg.SequenceNumber)
				if added {
					count = count + 1
					fmt.Println("Client: The Paxos system wrote value \"" + msg.RoundValue + "\" on round " + strconv.FormatInt((int64)(msg.RoundNumber), 10) + " and slot " + strconv.FormatInt((int64)(msg.SequenceNumber), 10))
				}
			} else {
				fmt.Println("Client: The Paxos system could not write value on round " + strconv.FormatInt((int64)(msg.RoundNumber), 10) + " with slot " + strconv.FormatInt((int64)(msg.SequenceNumber), 10))
			}

			if waitForAck {
				wait <- 1
			}

			if count >= 500 {
				eof <- 1
			}
		} else {
			connected = false
		}
	}
}

func onNodeListResponse() (list []nodelist.Node) {
	resp, ok := <-nodeListResponseChan

	if ok {
		list = resp.NodeList
	}

	return
}

func onLeaderIPResponse(ip string) string {
	node, ok := <-leaderIPResponseChan

	if ok {
		return node.IP
	}

	return ip
}

func readPort() (port string) {
	fmt.Println("System: Please enter port for the distributed system to work on")
	fmt.Print("System: ")
	fmt.Scanf("%s", &port)
	return
}

func getLocalIPAddress() (string, error) {
	var addrs, _ = net.InterfaceAddrs()
	for i := 0; i < len(addrs); i++ {
		if strings.Contains(addrs[i].String(), "/24") {
			return strings.Replace(addrs[i].String(), "/24", "", 1), nil
		}
	}

	return "", &IPAddressNotFound{"IP Address not found"}
}
