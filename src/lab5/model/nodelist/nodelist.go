package nodelist

import (
	"sync"
)

var Instance NodeList

type Node struct {
	IP          string
	Starttime   int64
	IsAlive     bool
	IsLeader    bool
	IsSuspected bool
}

type NodeList struct {
	List  []Node
	mutex sync.RWMutex
}

func init() {
	if Instance.List == nil {
		Instance = NodeList{List: make([]Node, 0)}
	}
}

func (n *NodeList) Add(node Node) {
	n.mutex.Lock()
	defer n.mutex.Unlock()

	for i := 0; i < len(n.List); i++ {
		if n.List[i].IP == node.IP {

			n.List[i].IsLeader = node.IsLeader
			n.List[i].Starttime = node.Starttime

			return
		}
	}

	n.List = append(n.List, node)
}

func (n *NodeList) Get(IP string) (node *Node) {
	n.mutex.Lock()
	defer n.mutex.Unlock()

	for i := 0; i < len(n.List); i++ {
		if n.List[i].IP == IP {
			return &n.List[i]
		}
	}

	return
}

func (n *NodeList) Contains(node Node) bool {
	n.mutex.Lock()
	defer n.mutex.Unlock()

	for i := 0; i < len(n.List); i++ {
		if n.List[i].IP == node.IP {
			return true
		}
	}

	return false
}

func (n *NodeList) IsEmpty() bool {
	n.mutex.Lock()
	defer n.mutex.Unlock()

	return len(n.List) <= 0
}
