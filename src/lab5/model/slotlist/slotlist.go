package slotlist

import (
	"sync"
	"time"
)

type SlotList struct {
	min   int
	size  int
	array []interface{}
	mutex sync.RWMutex
}

func NewSlotList() *SlotList {
	return &SlotList{min: 1, size: 0, array: make([]interface{}, 10)}
}

func (s *SlotList) Add(i interface{}, j int) bool {
	var idx = j - s.min

	if s.size <= idx {
		for k := s.size; k < idx*2+1; k++ {
			s.array = append(s.array, nil)
		}
	}

	if s.array[idx] == nil {
		s.size = idx
		s.array[idx] = i
		return true
	}

	return false
}

func (s *SlotList) Remove(j int) {
	var idx = j - s.min

	if s.size > idx {
		s.array[idx] = nil
	}
}

func (s *SlotList) Get(j int) interface{} {
	var idx = j - s.min
	if idx < s.size && s.array[idx] != nil {
		return s.array[idx]
	}

	return nil
}

func (s *SlotList) getFirst() interface{} {
	for s.array[0] == nil {
		time.Sleep(time.Second / 200)
	}

	return s.array[0]
}

func (s *SlotList) Pull() interface{} {
	first := s.getFirst()

	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.size = s.size - 1
	for i := 0; i < s.size; i++ {
		s.array[i] = s.array[i+1]
	}

	return first
}

func (s *SlotList) Size() int {
	return s.size + s.min
}

func (s *SlotList) UnfilledSlots() (u []int) {
	for i := 0; i < s.size; i++ {
		if s.array[i] == nil {
			u = append(u, i)
		}
	}

	return
}
