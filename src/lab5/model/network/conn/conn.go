package conn

import (
	"encoding/gob"
	"net"
	"sync"
)

type ConnectionList struct {
	mutex   sync.RWMutex
	connMap map[string]net.Conn
	decMap  map[string]*gob.Encoder
}

func NewConnectionList() *ConnectionList {
	return &ConnectionList{connMap: make(map[string]net.Conn, 0), decMap: make(map[string]*gob.Encoder, 0)}
}

func (list *ConnectionList) Add(ip string, conn net.Conn, encoder *gob.Encoder) {
	list.mutex.Lock()
	defer list.mutex.Unlock()

	if list.connMap[ip] == nil && list.decMap[ip] == nil {
		list.connMap[ip] = conn
		list.decMap[ip] = encoder
	}
}

func (list *ConnectionList) Remove(ip string) {
	list.mutex.Lock()
	defer list.mutex.Unlock()

	list.connMap[ip] = nil
	list.decMap[ip] = nil
}

func (list *ConnectionList) Get(ip string) (net.Conn, *gob.Encoder) {
	list.mutex.Lock()
	defer list.mutex.Unlock()

	return list.connMap[ip], list.decMap[ip]
}
