package tcp

import (
	"lab5/model/network/conn"

	"encoding/gob"
	"net"
	"strings"
	"sync"
)

type tcpMessage struct {
	IP      string
	Message interface{}
}

type TCPSocket struct {
	port           string
	connected      bool
	connectionList *conn.ConnectionList

	waitForChan chan int
	msgChan     chan tcpMessage

	mutex sync.RWMutex
}

func init() {
	gob.Register(tcpMessage{})
}

func NewTCPSocket(_port string, _connectionList *conn.ConnectionList) *TCPSocket {
	return &TCPSocket{port: _port, connected: true, connectionList: _connectionList, waitForChan: make(chan int, 0), msgChan: make(chan tcpMessage, 10)}
}

func (s *TCPSocket) Chan() chan tcpMessage {
	return s.msgChan
}

func (s *TCPSocket) Send(ip string, port string, msg interface{}) (err error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	conn, encoder := s.connectionList.Get(ip)

	if conn != nil && encoder != nil {
		encoder.Encode(&msg)
	} else {
		conn, e := net.Dial("tcp", strings.Join([]string{ip, ":", port}, ""))

		if e == nil {
			go s.nodeConnection(conn, true)
			<-s.waitForChan

			conn, encoder := s.connectionList.Get(ip)
			if conn != nil && encoder != nil {
				encoder.Encode(&msg)
			}
		} else {
			return e
		}
	}

	return nil
}

func (s *TCPSocket) StartSocket() {
	tcpAddr, err1 := net.ResolveTCPAddr("tcp", strings.Join([]string{":", s.port}, ""))

	if err1 == nil {
		listener, err2 := net.ListenTCP("tcp", tcpAddr)

		if err2 == nil {
			for s.connected {
				conn, err3 := listener.Accept()

				if err3 == nil {
					go s.nodeConnection(conn, false)
				}
			}
		}

		listener.Close()
	}
}

func (s *TCPSocket) nodeConnection(conn net.Conn, waitFor bool) {
	var connected = true
	addr := conn.RemoteAddr().String()
	ip := addr[0:strings.Index(addr, ":")]

	decoder := gob.NewDecoder(conn)
	encoder := gob.NewEncoder(conn)

	s.connectionList.Add(ip, conn, encoder)

	if waitFor {
		s.waitForChan <- 1
	}

	for s.connected && connected {
		var msg interface{}
		err4 := decoder.Decode(&msg)

		if err4 == nil {
			s.connected = s.send(s.msgChan, tcpMessage{IP: ip, Message: msg})
		} else {
			s.connectionList.Remove(ip)
			connected = false
		}
	}
}

func (s *TCPSocket) send(c chan tcpMessage, t tcpMessage) bool {
	defer func() { recover() }()
	c <- t
	return true
}
