package main

import "fmt"

func mapKeyValueTest() bool {
	someMap := make(map[int]string)
	someMap[0] = " String "
	_, ok := someMap[0]

	return ok
}

func main() {
	ok := mapKeyValueTest()
	fmt.Println(ok)
}
