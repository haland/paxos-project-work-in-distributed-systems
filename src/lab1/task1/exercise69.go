package main

import "fmt"
import "lab1/task1/tree"

func Same(t1, t2 *tree.Tree) bool {
	return tree.Compare(t1, t2)
}

func main() {
	t1 := tree.New(100, 1)
	t2 := tree.New(100, 1)
	t3 := tree.New(200, 1)

	fmt.Println(Same(t1, t2))
	fmt.Println(Same(t1, t3))
	fmt.Println(Same(t3, t2))
}
