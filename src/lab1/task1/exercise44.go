package main

import "fmt"
import "math"

func sqrt(x float64) float64 {
	v := 1.0
	lim := 0.00000001
	last_val := 0.0

	for math.Abs(v-last_val) > lim {
		last_val = v
		v -= (v*v - x) / (2 * x)
	}

	return math.Ceil(v)
}

func main() {
	fmt.Println(sqrt(16))
}
