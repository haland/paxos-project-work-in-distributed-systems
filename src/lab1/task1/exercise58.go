package main

import "fmt"
import "math"

type ErrNegativeSqrt struct {
	IllegalNumber float64
}

func (e *ErrNegativeSqrt) Error() string {
	return fmt.Sprintf("Cannot take the sqare root of a negative number: %v", e.IllegalNumber)
}

func sqrt(x float64) (float64, error) {
	if x < 0 {
		return x, &ErrNegativeSqrt{x}
	}

	v := 1.0
	lim := 0.00000001
	last_val := 0.0

	for math.Abs(v-last_val) > lim {
		last_val = v
		v -= (v*v - x) / (2 * x)
	}

	return math.Ceil(v), nil
}

func main() {
	val1 := 4.0
	sqrt1, err1 := sqrt(val1)
	if err1 != nil {
		fmt.Println(err1.Error())
	} else {
		fmt.Print("Square root of ")
		fmt.Print(val1)
		fmt.Println(" is:", sqrt1)
	}

	val2 := -4.0
	sqrt2, err2 := sqrt(val2)
	if err2 != nil {
		fmt.Println(err2.Error())
	} else {
		fmt.Print("Square root of ")
		fmt.Print(val2)
		fmt.Println(" is:", sqrt2)
	}
}
