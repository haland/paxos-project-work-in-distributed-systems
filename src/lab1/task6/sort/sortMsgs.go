//use: sortMsgs [file]
package main

import (
	"encoding/gob"
	"fmt"
	"lab1/task6/messages"
	"lab1/task6/optionalSort"
	"os"
)

var (
	semMsg  = make(chan int)
	semErr  = make(chan int)
	inChan  = make(chan interface{}, 5)     //channel from demarshaler to sort	
	msgChan = make(chan messages.StrMsg, 5) //message channel
	errChan = make(chan messages.ErrMsg, 5) //error channel
)

func main() {
	go handleMsgs(msgChan)
	go handleErrors(errChan)
	go optionalSort.Sort(inChan, msgChan, errChan)
	demarshal(os.Args[1], inChan)
	<-semMsg
	<-semErr
}

func demarshal(filePath string, inChan chan interface{}) {
	file, err1 := os.Open(filePath)

	if err1 != nil {
		fmt.Println("Error #1 occured on demarshal:")
	} else {
		decoder := gob.NewDecoder(file)
		buffer := make([]interface{}, 5)
		err2 := decoder.Decode(&buffer)

		if err2 != nil {
			fmt.Println("Error #2 occured on demarshal:", err2)
		}

		for _, i := range buffer {
			inChan <- i
		}

		close(inChan)
	}
}

func handleMsgs(inchan chan messages.StrMsg) {
	senders := ""

	for {
		msg, ok := <-inchan

		if ok {
			senders = senders + msg.Sender + ", "
			fmt.Println("Message received from", msg.Sender, ": ", msg.Content)
		} else {
			if senders != "" {
				fmt.Println("The senders: " + senders)
			}
			break
		}
	}

	semMsg <- 1
}

func handleErrors(inchan chan messages.ErrMsg) {
	errors := ""

	for {
		msg, ok := <-inchan

		if ok {
			errors = errors + msg.Error + ", "
			fmt.Println("Error received from", msg.Sender, ": ", msg.Error)
		} else {
			if errors != "" {
				fmt.Println("The errors: " + errors)
			}
			break
		}
	}

	semErr <- 1
}
