package optionalSort

import "lab1/task6/messages"

func Sort(inChan chan interface{}, msgChan chan messages.StrMsg, errChan chan messages.ErrMsg) {
	for {
		m, ok := <-inChan

		if ok {
			switch m.(type) {
			case messages.StrMsg:
				msgChan <- m.(messages.StrMsg)
			case messages.ErrMsg:
				errChan <- m.(messages.ErrMsg)
			}
		} else {
			break
		}
	}

	close(errChan)
	close(msgChan)
}
