package main

import "fmt"

func loop(n int) {
	for i := 0; i < n; i++ {
		fmt.Println("Round number", i+1)
	}
}

func main() {
	loop(10)
}
